<?php
session_cache_limiter('none');			//This prevents a Chrome error when using the back button to return to this page.
session_start();
$inUsername = $_SESSION['userName1'];   //get username
 
	if ($_SESSION['validUser1'] == "yes"){	                    //if valid user or already signed on, skip the rest
		$messageSuccess = "Welcome Back <em>". $inUsername."</em> !";	//Create greeting for VIEW area		
	}else{
		if (isset($_POST['submitLogin']) ){			            //if login portion was submitted do the following
		
			$inUsername = $_POST['event_user_name'];	       //pull the username from the form
			$inPassword = $_POST['event_user_password'];	   //pull the password from the form
			$_SESSION['userName1'] = $inUsername;
			
			include 'HomeworkPageFiles/connectPDO.php';		  //Connect to the database

			$sql = "SELECT event_user_name,event_user_password FROM event_user WHERE event_user_name = :username AND event_user_password = :password"; 	
			
			$query = $conn->prepare($sql);                   //prepare the query
			
			$query->bindParam(':username', $inUsername);     //bind parameters to prepared statement
			$query->bindParam(':password', $inPassword);
			
			$query->execute();
			
			$query->fetch();	
			
			if ($query->rowCount() == 1 ){		            //If this is a valid user there should be ONE row only
				$_SESSION['validUser1'] = "yes";				//this is a valid user so set your SESSION variable
				$messageSuccess = "Welcome Back <em>". $inUsername."</em> !";
			}else{
				$_SESSION['validUser1'] = "no";				//error in login, not valid user	
				$messageError = "<em style='color:#b20000;'>Incorrect Username or Password. <br>Please try again.</em>";
			}			
			
			$conn = null;
			
		}else{
			//user needs to see form
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PHP Final Project - Furniture Co</title>
  <!-- Bootstrap core CSS -->
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
  <!--Text Styles Google Fonts-->
  <link href="https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i" rel="stylesheet">  
  <!--Icons Font Awesome-->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!--Custom CSS--> 
  <link href="HomeworkPageFiles/FPstyles.css" rel="stylesheet">
  <link href="HomeworkPageFiles/FPloginStyles.css" rel="stylesheet">
</head>
<body>

<?php
	if ($_SESSION['validUser'] == "yes"){	//if this is a valid user then show them the Administrator Page	
?>
		<nav class="navbar navbar-fixed-top">
		  <div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="FPindex.php"><img src="HomeworkPageImages/ChairLogo3.png" width="30" height="33" class="d-inline-block align-left"/> Furniture Co </a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">	
				<li><a id="greeting">Administrator Options</a></li>		
				<li><a href="FPaddProductForm.php">Add Product</a></li>
				<li><a href="FPselectProduct.php">View/Select Products</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a id="greeting"><?php echo $messageSuccess;?></a></li>
				<li><a href="FPlogin.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
				<li><a href="FPlogout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			  </ul>
			</div><!--/.navbar-collapse -->
		  </div>
		</nav>

<?php
	}else{	//if user is not a valid user then display login
?>
		
		<h2><a href="FPindex.php"><img src="HomeworkPageImages/ChairLogo3.png" width="101" height="113"/>Furniture Co</a></h2>
		<h3><?php echo $messageError;?></h3>
		
		<div id="loginContainer">
			<h3>Administrator Login</h3>
            <form method="post" name="loginForm" action="FPlogin.php" >
                <p class="label">Username:
					<input name="event_user_name" type="text" class="form-control"/>
				</p> 
                <p class="label">Password:
					<input name="event_user_password" type="password" class="form-control" />
				</p> 
                <p class="formButtons">
					<input name="submitLogin" value="LOGIN" type="submit" class="btn"/> 
					<input name="" type="reset" value="RESET" class="btn"/>&nbsp;
				</p>
            </form>
		</div>
                
<?php 
	}  //end of checking for a valid user			
?>
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>