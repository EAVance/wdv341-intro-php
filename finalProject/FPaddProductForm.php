<?php
session_start();
$inUsername = $_SESSION['userName1'];   //get username

if ($_SESSION['validUser1'] == "yes") {        //if valid user you have access to this page               
	$messageSuccess = "Welcome Back <em>". $inUsername."</em> !";    //welcome message
	
	$inName = $inDescription = $inImage = $inUpdateImage = $inActive = $inDate = $inID = $inPhone = $productImageName = "";    
	$nameError = $descriptionError = $imageError = $imageUploadError = $activeError = $message = $e = "";  
	$formTitle = "Add Product";
	$validForm = false;
	require 'HomeworkPageFiles/connectPDO.php';	//CONNECT to the database

	if(isset($_POST["submit"])){ //-------------IF ADD FORM HAS HAS BEEN SUBMITTED, GATHER INPUT, START VALIDATIONS--------------   
		$inName = $_POST['product_name'];
		$inDescription = $_POST['product_description'];
		$inImage = $_FILES["photo"]["name"]; 
		$inActive = $_POST['product_active'];
		$inID = $_POST['product_id'];	      //hidden field used for update form
		$inPhone = $_POST['Phone'];
		
		if($inID != ""){                     //if there is an product_id change form header to UPDATE
			$formTitle = "Update Product";   
		}else{
			$formTitle = "Add Product";   //if there is NO product_id change form header to ADD
		}
		
		//validating name, if empty or just spaces - form is invalid & error message displays, else sanitize string
		function validateName(){           
			global $validForm, $nameError, $inName;		
			if(trim($inName) == ""){
				$validForm = false;
				$nameError = "Product Name is Required.";
			}else{
				$inName = filter_var($inName, FILTER_SANITIZE_STRING);
			}
		}
		
		//validating description, if empty or spaces - form is invalid & error message displays, else sanitize string
		function validateDescription(){
			global $validForm, $descriptionError, $inDescription;
			if(trim($inDescription) == ""){
				$validForm = false;
				$descriptionError = "Product Description is Required.";
			}else{
				$inDescription = filter_var($inDescription, FILTER_SANITIZE_STRING);
			}
		}
	
		//get name of previous uploaded image
		$stmt2 = $conn->prepare('SELECT product_image_name FROM furniture_products WHERE product_id = ?');	
		$stmt2->execute([$inID]);          //binding variable inID by passing them as an array within execute statement
		$productImageName = $stmt2->fetchColumn();
				
		//validate upload image
		function validateUploadImage(){
			global $validForm, $imageError, $imageUploadError, $inImage, $inUpdateImage, $productImageName;

			
			if($inImage == "" && $productImageName == ""){     //if this is a new add product image then an image file has to be uploaded
				$validForm = false;
				$imageError = "Please Select Image";
			}else if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){            //if updating image file or adding new product with image do this
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
				$filename = $_FILES["photo"]["name"];
				$filetype = $_FILES["photo"]["type"];
				$filesize = $_FILES["photo"]["size"];
				
				// Verify file extension
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
				
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
				
				// Verify MYME type of the file
				if(in_array($filetype, $allowed)){
					// Check whether file exists before uploading it
					if(file_exists("HomeworkPageImages/database_product_images/" . $_FILES["photo"]["name"])){
						$validForm = false;
						$imageUploadError = $_FILES["photo"]["name"] . " already exists in desired folder.";
					}else{
						move_uploaded_file($_FILES["photo"]["tmp_name"], "HomeworkPageImages/database_product_images/" . $_FILES["photo"]["name"]);  //move image file to images folder
						unlink("HomeworkPageImages/database_product_images/".$productImageName);       //delete previous image from folder
						$productImageName = '';    //empty previous image var
					}
				}else{
					$validForm = false;
					$imageUploadError = "Error: There was a problem uploading your file. Please try again."; 
				}
			}else if($_FILES["photo"]["error"] == 1){
				$validForm = false;
				$imageUploadError = "Error: " . $_FILES["photo"]["error"];
			}else{
				
			}	
		}
	
		//validating product active status, if both unchecked - form is invalid & error message displays
		function validateActive(){           
			global $validForm, $activeError, $inActive;		
			if(!isset($inActive)){
				$validForm = false;
				$activeError = "Please Select Product Active Status.";
			}
		}
		
		//date of product add or update
		function productAddDate(){
			global $inDate;
			date_default_timezone_set('America/Chicago');
			$inDate = date('m/d/Y');
		}
		
		//validate phone, if not blank - form is invalid
		function validatePhony(){
			global $validForm, $inPhone;
			if($inPhone){
				$validForm = false;
			}
		}
	
		$validForm = true;       
		//calling validation functions
		validateName(); 
		validateDescription();
		validateActive();
		productAddDate();
		validatePhony();
		validateUploadImage();
			
			
		if($validForm){ //-----------------IF VALID FORM MOVE CONTENT TO DATABASE--------------------
				
			if($inID != ""){ //------------IF PRODUCT UPDATE DO THIS----------------
				if($inImage != ""){       //IF UPDATING IMAGE - Create this Update SQL command string
					try {
						$updateSQL = "UPDATE furniture_products SET product_name= :productName, product_description= :productDescription, product_image_name= :productImage, date_added= :dateAdded, product_active= :productActive WHERE product_id = :productId";
				
						//PREPARE the SQL statement
						$stmt = $conn->prepare($updateSQL);
						
						//BIND the values to the input parameters of the prepared statement
						$stmt->bindParam(':productName', $inName);
						$stmt->bindParam(':productDescription', $inDescription);		
						$stmt->bindParam(':productImage', $inImage);		
						$stmt->bindParam(':dateAdded', $inDate);		
						$stmt->bindParam(':productActive', $inActive);
						$stmt->bindParam(':productId', $inID);
						
						//EXECUTE the prepared statement
						$stmt->execute();
						$message = "<span class='check'>&#x2714;</span>Product Update Successful";
						
						$conn = null;
					}catch(PDOException $e){
						$message = "<span class='X'>&#x2718;</span> There has been a problem. Please try again later."; 
					}
				}else{	            //IF NOT UPDATING IMAGE - Create this Update SQL command string
					try {
						$updateSQL2 = "UPDATE furniture_products SET product_name= :productName, product_description= :productDescription, date_added= :dateAdded, product_active= :productActive WHERE product_id = :productId";
						
						//PREPARE the SQL statement
						$stmt2 = $conn->prepare($updateSQL2);
						
						//BIND the values to the input parameters of the prepared statement
						$stmt2->bindParam(':productName', $inName);
						$stmt2->bindParam(':productDescription', $inDescription);				
						$stmt2->bindParam(':dateAdded', $inDate);		
						$stmt2->bindParam(':productActive', $inActive);
						$stmt2->bindParam(':productId', $inID);
						
						
						//EXECUTE the prepared statement
						$stmt2->execute();
						$message = "<span class='check'>&#x2714;</span>Product Update Successful";
						
						$conn = null;
					}catch(PDOException $e){
						$message = "<span class='X'>&#x2718;</span> There has been a problem. Please try again later."; 
					}
				}
			}else{ //------------------IF NEW PRODUCT INSERT DO THIS------------------
				
				try {
					//Create the SQL command string
					$sql = "INSERT INTO furniture_products (";    
					$sql .= "product_name, ";             
					$sql .= "product_description, ";
					$sql .= "product_image_name, ";
					$sql .= "date_added, ";
					$sql .= "product_active ";   
					$sql .= ") VALUES (:productName, :productDescription, :productImage, :dateAdded, :productActive)";
					
					//PREPARE the SQL statement
					$stmt = $conn->prepare($sql);
					
					//BIND the values to the input parameters of the prepared statement
					$stmt->bindParam(':productName', $inName);
					$stmt->bindParam(':productDescription', $inDescription);		
					$stmt->bindParam(':productImage', $inImage);		
					$stmt->bindParam(':dateAdded', $inDate);		
					$stmt->bindParam(':productActive', $inActive);	
					
					//EXECUTE the prepared statement
					$stmt->execute();	
					$message = "<span class='check'>&#x2714;</span>Product Successfully Added";
					
					$conn = null;
				}catch(PDOException $e){
					$message = "<span class='X'>&#x2718;</span> There has been a problem. Please try again later.";
				}
			}	
		}else{ //----------------------IF INVALID FORM DISPLAY ERROR MESSAGE & FORM--------------------------
			$message = "<span style='color:#b20000;'>Invalid Entry. Please Try Again.</span>";
		}//ends check for valid form	
	}else{ //---------------------IF FORM HAS NOT BEEN SEEN AND NOT SUBMITTED DO THIS------------------------
		$inID = $_GET['product_id'];
		
		if($inID != ""){                    //if there is an product_id change form header to UPDATE
			$formTitle = "Update Product";
		}else{                              //if there is no product_id change form header to ADD
			$formTitle = "Add Product";
		}
		
		if(isset($inID)){ //-------------------IF PRODUCT ID IS PRESENT FROM $_GET, SHOW PRODUCT DATA FOR UPDATE----------------
		    
			try {
				$stmt = $conn->prepare('SELECT product_name, product_description, product_image_name, product_active FROM furniture_products WHERE product_id = :productID');
				$stmt->bindParam(':productID', $inID);
				$stmt->execute();
				$row = $stmt->fetch();
				if($row){
					$inName = $row["product_name"];
					$inDescription = $row["product_description"];
					$inUpdateImage = $row["product_image_name"]; 
					$inActive = $row["product_active"];
				}else {
					$displayErrorMsg = "<h3><span class='X'>&#x2718;</span><em> Zero results were found</em></h3>";			
				}		
				
				$conn = null;
			}catch(PDOException $e){
				$displayErrorMsg = "<h3><span class='X'>&#x2718;</span><em> Sorry there has been a problem.</em></h3>";
			}
		}else{ 
		
		}
	}
	//if not a valid user redirect to login page
}else{
	header('Location: FPlogin.php');         
}
;?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP Final Project - Furniture Co</title>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
	<link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
	<!--Text Styles Google Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i" rel="stylesheet">	
	<!--Custom CSS--> 
	<link href="HomeworkPageFiles/FPstyles.css" rel="stylesheet">
	<link href="HomeworkPageFiles/FPaddProductStyles.css" rel="stylesheet">
	<!--JS-->
	<script src="HomeworkPageFiles/jquery-3.2.1.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-fixed-top"> 
		<div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="FPindex.php"><img src="HomeworkPageImages/ChairLogo3.png" width="30" height="33" class="d-inline-block align-left"/> Furniture Co </a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">	
				<li><a id="greeting">Administrator Options</a></li>		
				<li class="active"><a href="FPaddProductForm.php">Add Product</a></li>
				<li><a href="FPselectProduct.php">View/Select Products</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a id="greeting"><?php echo $messageSuccess?></a></li>
				<li><a href="FPlogin.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
				<li><a href="FPlogout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			  </ul>
			</div><!--/.navbar-collapse -->
		</div>
	</nav>
	
	<div class="container mainContent">
		<div class="row">
	  
<?php      
	if ($validForm) {      //if valid form remove form and display message	
?>
			
				<div id="submitSuccess">
					<div class="msg">
						<h3><em><?php echo $message; ?></em></h3> 
					</div>
					
					<?php
						if($e == ""){     //if valid form but there are database errors don't display input data
					?>
						<div id="custInput">
							<ul>
								<li><strong>Product Name: </strong> <em><?php echo $inName;?></em></li>
								<li><strong>Product Description: </strong> <em><?php echo $inDescription;?></em></li>
								<li><strong>Product Image Name: </strong> <em><?php echo $inImage;?><?php echo $productImageName;?></em></li>
								<li><strong>Date Added: </strong> <em><?php echo $inDate;?></em></li>
								<li><strong>Product Active: </strong> <em><?php echo $inActive;?></em></li>
							</ul>
						</div>
					<?php	 		
						}
					?>
					
				</div>
				
<?php	 		
	}else {           //if not valid form display form again with previously entered product info
?>
			
				<h2><?php echo $formTitle;?></h2>
				<h3><em><?php echo $message; ?></em></h3> 
		 
				<!--Form section-->		
				<form  id="productForm" name="productForm" method="post"  enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
					
					<p>
						<label>Product Name <br><span class="error"><?php echo $nameError; ?></span></label>
						<input type="text" name="product_name" id="eventName" class="form-control" value="<?php echo $inName;?>">
					</p>
					<p>
						<label>Product Description <br><span class="error"><?php echo $descriptionError; ?></span></label>
						<textarea  name="product_description" class="form-control" id="description" rows="5" ><?php echo $inDescription;?></textarea>
					</p>
					<p>
						<label>Select Product Image<br><span class="error"><?php echo $imageError; ?><?php echo $imageUploadError; ?></span></label>
						<br><span class="prevUploadedImage">Previous Uploaded Image: <em><?php echo $inUpdateImage;?></em></span>
						<input type="file" name="photo" id="fileSelect">
						<label for="fileSelect"></label>
						<span class="note"><em> Only .jpg, .jpeg, .gif, .png formats allowed. <br>Max size 5 MB</em></span>
					</p>	
					<p>	
						<label>Do you want this product to be currently active? <br><span class="error"><?php echo $activeError; ?></span></label>
						<span class="radio">
							<input type="radio" name="product_active" value="yes" <?php if ($inActive == 'yes') { echo "checked"; } ?>/> Yes
							<input type="radio" name="product_active" value="no" <?php if ($inActive == 'no') { echo "checked"; } ?>/> No 
						</span>
					</p>
						<input type="hidden" name="product_id" id="id" value="<?php echo $inID; ?>" />
						<div id="phony">
							<label>Phone:</label>
							<input type="text" name="Phone">
						</div>											
					
					<div class="formButtons">
						<input type="submit" name="submit" value="SUBMIT" id="submit" class="btn">
						<input type="reset" name="reset" value="RESET" id="reset" class="btn">
					</div>
			
				</form><!--end form-->
<?php
	}   // end else 
?>
		</div><!--end row-->
	</div> <!-- /container -->
	
	 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>