<?php
session_start();
$inUsername = $_SESSION['userName1']; //get user name

//no valid user needed, everyone has access to this view page

if($inUsername != ''){
	$messageSuccess = "Welcome Back <em>". $inUsername."</em> !";   //if admin is logged in then display welcome message with username
}else{
	$messageSuccess = "";           //else do not display anything
}
	require 'HomeworkPageFiles/connectPDO.php';	   //database connect
	$displayErrorMsg = "";
	
	try {                           //get products from database that are set to active
		$sql = $conn->prepare("SELECT product_name, product_description, product_image_name FROM furniture_products WHERE product_active = 'yes'");
		$sql->execute();
		$count = $sql->rowCount(); 
		
	}catch(PDOException $e){
		$displayErrorMsg = "<em>Sorry there has been a problem.</em>";
	}
	
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PHP Final Project - Furniture Co</title>
  <!-- Bootstrap core CSS -->
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
  <!--Text Styles Google Fonts-->
  <link href="https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i" rel="stylesheet">
  <!--Icons Font Awesome-->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!--Custom CSS--> 
  <link href="HomeworkPageFiles/FPstyles.css" rel="stylesheet">
  <script src='HomeworkPageFiles/jquery-3.2.1.min.js'></script>
  <script>
		$(document).ready(function() {
		//on click of up arrow animated scroll to top
			$('#topButton').click(function() {
				$("html, body").animate({     //body is used by webkit browsers, html is used by firefox
					scrollTop:0 
				}, 1000)                      //speed
			}); 
		});
  </script>
</head>
<body>

	<nav class="navbar navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="FPindex.php" ><img src="HomeworkPageImages/ChairLogo3.png" width="30" height="33" class="d-inline-block align-left"/> Furniture Co </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
			<li class="active"><a href="FPindex.php">Designs</a></li>		
			<li><a href="#">About</a></li>
			<li><a href="#">Showroom</a></li>
			<li><a href="FPcontact.php">Contact</a></li>
		  </ul>
		  <ul class="nav navbar-nav navbar-right">
			<li><a id="greeting"><?php echo $messageSuccess?></a></li>
			<li><a href="FPlogin.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
			<li><a href="FPlogout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
		  </ul>
        </div><!--/.navbar-collapse -->
      </div> 
    </nav>
	
    <!-- Main jumbotron-->
    <div class="jumbotron">
      <div class="container">
        <h1><img src="HomeworkPageImages/ChairLogo3.png" width="101" height="113"/>Furniture Co</h1>
        <p>As we evolve, our homes should, too.</p>
      </div>
	  <div class="d-block p-3 contactBlock">
		<div class="col-12">
			<h2>Furniture Designs</h2>
		</div>
	  </div>
    </div>
	 
	 
<!---------------------------------------PRODUCT CARDS---------------------------------------->
    <div class="container mainContent">
		<div class="row">
	 
<?php
	//Display each row as formatted output
	while( $row = $sql->fetch()){
		$displayProductName = $row["product_name"];
		$displayProductDescription = $row["product_description"];
		$displayProductImage = $row["product_image_name"]; 
?>	

			<div class = "col-sm-6">
			 <div class="card">
			  <img class="image img-responsive" src="HomeworkPageImages/database_product_images/<?php echo $displayProductImage; ?>" alt='' title='' >
			  <div class="cardContent">
				<h2><?php echo $displayProductName; ?></h2>
				<p><?php echo $displayProductDescription; ?></p>
				<p class="cardBtn"><a class="btn" href="FPcontact.php" role="button">Custom Order &raquo;</a></p>
			  </div>
			 </div>
			</div>
	
<?php
  	}//close while loop
	$conn = null;
?>	
<!---------------------------------------END CARDS---------------------------------------------->	
			<h3><?php echo $displayErrorMsg; ?></h3>
			
		</div> <!-- /row -->
	</div> <!-- /container -->
	 
		<p id="btn2"><a id="topButton" href="#">^</a></p>
	 
		<div class="d-block p-3 contactBlock">
			<div class="col-12">
				<h6><strong><em>Get connected</em></strong>
					<!--Facebook-->
					<a><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
					<!--Twitter-->
					<a><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
					<!--Linkedin-->
					<a><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a>
					<!--Instagram-->
					<a><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
				</h6>
			</div>
		</div>
		
		<!--Footer-->
		<footer>
			<!--Footer Links-->
			<div class="container text-center">
				<div class="row">
					<!--First column-->
					<div class="col-md-3 col-lg-4 col-xl-3 ">
						<h6><a href="FPindex.php" id="brand"><strong>Furniture Co</strong></a></h6>
						<hr style="width: 60px;">
						<p class="slogan">As we evolve, <br>our homes should, too.</p>
					</div>
					<!--Second column-->
					<div class="col-md-2 col-lg-2 col-xl-2 ">
						<h6><strong>Useful Links</strong></h6>
						<hr style="width: 60px;">
						<p><a href="FPindex.php">Designs</a></p>
						<p><a href="#">Showroom</a></p>
						<p><a href="#">About</a></p>
						<p><a href="FPcontact.php">Contact</a></p>
					</div>
					<!--Third column-->
					<div class="col-md-3 col-lg-2 col-xl-2 ">
						<h6><strong>Showroom Hours</strong></h6>
						<hr style="width: 60px;">
						<p>Mon <em>closed</em></p>
						<p>Tues-Thur 9am - 6pm</p>
						<p>Fri 9am - 8pm</p>
						<p>Sat &amp; Sun 8am - 9pm</p>
					</div>
					<!--Fourth column-->
					<div class="col-md-4 col-lg-3 col-xl-3">
						<h6><strong>Contact</strong></h6>
						<hr style="width: 60px;">
						<p> New York, NY 10012, US</p>
						<p> info@example.com</p>
						<p> + 01 234 567 88</p>
						<p> + 01 234 567 89</p>
					</div>
				</div>
			</div>
			<!-- Copyright-->
			<div id="copyright">
				<div class="container-fluid">
					© 2017 Copyright: <a href="FPindex.php"><strong> Furniture Co</strong></a>
				</div>
			</div>
		</footer>
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
   
</body>
</html>