<?php
session_start();
$inUsername = $_SESSION['userName1'];  //get user name

if ($_SESSION['validUser1'] == "yes") {   //if valid user or already signed on you have access to this page
	
	$messageSuccess = "Welcome Back <em>". $inUsername."</em> !";    //welcome message
	
	require 'HomeworkPageFiles/connectPDO.php';	  //database connection
	$displayMsg = $displayErrorMsg = "";
	
	try {     //get all furniture products in database and display in a table for admin to view or select
		$result = $conn->prepare('SELECT product_id, product_name, product_description, product_image_name, date_added, product_active FROM furniture_products');
		$result->execute();
		if($result->rowCount() > 0){
			$displayMsg .= "<table><tr class='tableHeader'><td>ID</td><td>Name</td><td>Description</td><td>Image Name</td><td>Date Added</td><td>Active</td></tr>";
			while($row = $result->fetch()) {
				$displayMsg .= "<tr><td>".$row['product_id']."</td>";
				$displayMsg .= "<td>".$row["product_name"]."</td>";
				$displayMsg .= "<td>".$row["product_description"]."</td>";
				$displayMsg .= "<td>".$row["product_image_name"]."</td>";
				$displayMsg .= "<td>".$row["date_added"]."</td>";
				$displayMsg .= "<td>".$row["product_active"]."</td>"; 
				$displayMsg .= "<td><a href='FPaddProductForm.php?product_id=".$row['product_id']."' class='tooltip'><i class='fa fa-pencil fa-lg' aria-hidden='true'></i> <span class='tooltiptext'>Edit</span></a></td>"; 
				$displayMsg .= "<td><a href='FPdeleteProduct.php?product_id=".$row['product_id']."' class='tooltip'><i class='fa fa-trash-o fa-lg' aria-hidden='true'></i> <span class='tooltiptext'>Delete</span></a></td></tr>";
			}
			$displayMsg .= "</table>";
		}else {
			$displayErrorMsg = "<h3><em>Zero results were found</em></h3>";		 //no results found in database message	
		}	
	}catch(PDOException $e){
		$displayErrorMsg = "<h3><em>Sorry there has been a problem.</em></h3>";  //other database error message
	}
	$conn = null;
	//if you are not a valid user redirect back to login page
}else{
	header('Location: FPlogin.php');      
}
;?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP Final Project - Furniture Co</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
	<link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
	<!--Text Styles Google Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i" rel="stylesheet">
	<!--Custom CSS--> 
	<link href="HomeworkPageFiles/FPstyles.css" rel="stylesheet">
	<link href="HomeworkPageFiles/FPselectStyles.css" rel="stylesheet">
	<script src='HomeworkPageFiles/jquery-3.2.1.min.js'></script>
	<script>
		$(document).ready(function() {
		//on click of up arrow animated scroll to top
			$('#topButton').click(function() {
				$("html, body").animate({         //body is used by webkit browsers, html is used by firefox
					scrollTop:0 
				}, 1000)                        //speed
			});
		});
	</script>
</head>
<body>

	<nav class="navbar navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="FPindex.php"><img src="HomeworkPageImages/ChairLogo3.png" width="30" height="33" class="d-inline-block align-left"/> Furniture Co </a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">	
				<li><a id="greeting">Administrator Options</a></li>		
				<li><a href="FPaddProductForm.php">Add Product</a></li>
				<li class="active"><a href="FPselectProduct.php">View/Select Products</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a id="greeting"><?php echo $messageSuccess?></a></li>
				<li><a href="FPlogin.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
				<li><a href="FPlogout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			  </ul>
			</div><!--/.navbar-collapse -->
		</div>
	</nav>
	
	<div class="container mainContent">
		<div class="row">
		
<?php      
	if ($result->rowCount() > 0) {	   //if query results have more than 0 records in database the products will show	
?>
			<h2>Products</h2>
			<div id="content">
				<?php echo $displayMsg; ?>
			</div>
	
<?php	 		
	}else {                           //else if the query results are 0 records in database or error, message will show
			echo $displayErrorMsg;
	} // end else 
?>

			<p id="btn2"><a id="topButton" href="#">^</a></p>

		</div><!--end row-->
	</div> <!-- /container -->
	
	 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>