<?php
session_start();
$inUsername = $_SESSION['userName1'];     //get username

//no valid user needed, everyone has access to this view page

if($inUsername != ''){
	$messageSuccess = "Welcome Back <em>". $inUsername."</em> !";    //if admin is logged in then display welcome message with username
}else{
	$messageSuccess = "";           //else do not display anything
}

include 'HomeworkPageFiles/Email1.php';   //include email class

$inName = $inEmail = $inContactReason = $inComments = $formSubmitDate = $formSubmitTime = $inMailList = $inProductInfo = $inPhone = "";    
$nameError = $emailError = $contactReasonError = $commentsError = $message = "";   
$validForm = false;

	//if form has been submitted gather user input and start validations
	if(isset($_POST["submit"])){         
		$inName = $_POST['contact_name'];
		$inEmail = $_POST['contact_email'];
		$inContactReason = $_POST['contact_reason']; 
		$inComments = $_POST['contact_comments'];
		$inMailList = (isset($_POST['contact_newsletter'])) ? 'Yes' : 'No';         //if checkbox is checked, yes is sent to database,if not checked, no is sent to database
		$inProductInfo = (isset($_POST['contact_more_products'])) ? 'Yes' : 'No';   //if checkbox is checked, yes is sent to database,if not checked, no is sent to database
		$inPhone = $_POST['Phone'];
		
		//validating name, if empty or just spaces - form is invalid & error message displays, else sanitize string
		function validateName(){           
			global $validForm, $nameError, $inName;		
			if(trim($inName) == ""){
				$validForm = false;
				$nameError = "Name is Required.";
			}else{
				return $inName = filter_var($inName, FILTER_SANITIZE_STRING);
			}
		}
		
		//validating email, if empty or spaces OR not a valid email - form is invalid & error message displays
		function validateEmail(){
			global $validForm, $emailError, $inEmail;
			if(trim($inEmail) == ""){
				$validForm = false;
				$emailError = "Email is Required.";
			}else if(!preg_match("/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/",trim($inEmail))){        
				$validForm = false;
				$emailError = "Invalid Email (eg. yourName@yahoo.com)";
			}
		}
	
		//validating contact reason, if unselected - form is invalid & error message displays
		function validateContactReason(){               
			global $validForm, $contactReasonError, $inContactReason;
			if($inContactReason == "default"){
				$validForm = false;
				$contactReasonError = "Please Select a Contact Reason<br>";
			}
		}
		
		//validating comments, not required unless user selects 'Other' for contact reason, else sanitize string
		function validateComments(){
			global $validForm, $commentsError, $inContactReason, $inComments;
			if($inContactReason == "Other" && trim($inComments) == ""){
				$validForm = false;
				$commentsError = "By selecting 'Other' for contact reason, you must enter a comment.";
			}else{
				return $inComments = filter_var($inComments, FILTER_SANITIZE_STRING);
			}
		}
		
		function validatePhony(){
			global $validForm, $inPhone;
			if($inPhone){
				$validForm = false;
			}
		}
	
		//date and time of form submission
		function formSubmitDate(){
			global $formSubmitDate,$formSubmitTime;
			date_default_timezone_set('America/Chicago');
			$formSubmitDate = date('m/d/y');
			$formSubmitTime = date('h:i A');
		}
		
		$validForm = true;       
		//calling validation functions
		validateName(); 
		validateEmail();
		validateContactReason();
		validatePhony();
		validateComments(); 
		formSubmitDate();		
		
		//Email set up for 'user/customer' confirmation email 
		$custConfirmationEmail = new Email();               //new Email Object
		$custConfirmationEmail->setRecipient($inEmail);    
		$custConfirmationEmail->setSender("contact@erinavance.info");         
		$custConfirmationEmail->setSubject("Furniture Co Confirmation");
		$custConfirmationEmail->setMessage("
			<html>
			<head>
				<link href='https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i' rel='stylesheet'>  
				<style>
					body{
						font-family: 'Comfortaa', cursive;}
					#header {
						text-align:center;
						margin:.09em; 
						color:black;
						font-family: 'Numans', sans-serif;
						letter-spacing:0.04em;}
					#header img{
						vertical-align:middle;}
					#container{
						background-color:#D9D9D9;
						padding:5% 10%;
						}
					#main {
						width:50%;
						color:#1A292E;
						margin:0 auto;
						letter-spacing:0.04em;}
					ul {
						list-style-type:none;
						padding-left:0px;}
					h3{
						font-family: 'Numans', sans-serif;
						color:#337ab7;}
					@media only screen and (max-width:991px){
						#main{
							width:100%;
						}
					}
				</style>
			</head>
			<body>
			  <div id='header'>
				<h2><img src='http://erinavance.info/courses/WDV341/WDV341Homework/Final/HomeworkPageImages/ChairLogo3.png' width='101' height='113'/>Furniture Co</h2>
			  </div>
			  <div id='container'>
				  <div id='main'>
					<h3>Dear <em style='color:#337ab7;'>".$inName."</em>,</h3>
					<h4>Thank you for your recent comments or questions. We will review your input and be in contact with you shortly.</h4>
					<h4>You have successfully submitted the following information:</h4>
					<ul>
						<li><strong>Name:</strong> <em>".$inName."</em></li>
						<li><strong>Email:</strong> <em>".$inEmail."</em></li>
						<li><strong>Contact Reason:</strong> <em>".$inContactReason."</em></li>
						<li><strong>Comments:</strong> <em>".$inComments."</em></li>
						<li><strong>Mailing List:</strong> <em>".$inMailList."</em></li>
						<li><strong>More Information:</strong> <em>".$inProductInfo."</em></li>
						<li><strong>Date:</strong> <em>".$formSubmitDate."</em></li>
						<li><strong>Time:</strong> <em>".$formSubmitTime."</em></li>
					</ul>
					<h3><em>Furniture Co <br>Customer Service</em></h3>
				  </div>
			  </div>
			</body>
			</html>");
		
		//Email set up with customer information sent to 'website/business owner' 
		$custServiceEmail = new Email();                             //new Email Object
		$custServiceEmail->setRecipient("ealippert@dmacc.edu");    
		$custServiceEmail->setSender("contact@erinavance.info");         
		$custServiceEmail->setSubject("Furniture Co Customer Contact Form Submission");
		$custServiceEmail->setMessage("
			<html>
			<head>
				<link href='https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i' rel='stylesheet'>  
				<style>
					body{
						font-family: 'Comfortaa', cursive;}
					#header {
						text-align:center;
						margin:.09em; 
						color:black;
						font-family: 'Numans', sans-serif;
						letter-spacing:0.04em;}
					#header img{
						vertical-align:middle;}
					#container{
						background-color:#D9D9D9;
						padding:5% 10%;}
					#main{
						width:50%;
						letter-spacing:0.04em;
						color:#1A292E;
						margin:0 auto;}
					ul {
						list-style-type:none;
						padding-left:0px;}
					li {
						border-bottom: 1px solid #c2c6c6;
						padding:4px;}
					h3{
						font-family: 'Numans', sans-serif;
						color:#337ab7;
						text-align:center;}
					@media only screen and (max-width:768px){
						#main{
							width:100%;
						}
					}
				</style>
			</head>
			<body>
			  <div id='header'>
				<h2><img src='http://erinavance.info/courses/WDV341/WDV341Homework/Final/HomeworkPageImages/ChairLogo3.png' width='101' height='113'/>Furniture Co</h2>
			  </div>
			  <div id='container'>
				  <div id='main'>
					<h3>Customer Information:</h3>
					<ul>
						<li><strong>Name:</strong> <em>".$inName."</em></li>
						<li><strong>Email:</strong> <em>".$inEmail."</em></li>
						<li><strong>Contact Reason:</strong> <em>".$inContactReason."</em></li>
						<li><strong>Comments:</strong> <em>".$inComments."</em></li>
						<li><strong>Mailing List:</strong> <em>".$inMailList."</em></li>
						<li><strong>More Information:</strong> <em>".$inProductInfo."</em></li>
						<li><strong>Date:</strong> <em>".$formSubmitDate."</em></li>
						<li><strong>Time:</strong> <em>".$formSubmitTime."</em></li>
					</ul>
				  </div>
			  </div>
			</body>
			</html>");
		
		//if valid form submit customer info to database
		if($validForm){
			$custConfirmationEmail->sendMail();  //because form is valid send a confirmation email to customer - using Email class
			$custServiceEmail->sendMail();       //because form is valid send customer service customers submitted information - using Email class
				
			try {
				require 'HomeworkPageFiles/connectPDO.php';	//CONNECT to the database		
				
				//Create the SQL command string
				$sql = "INSERT INTO customer_contacts(";    
				$sql .= "contact_name, ";             
				$sql .= "contact_email, ";
				$sql .= "contact_reason, ";
				$sql .= "contact_comments, ";
				$sql .= "contact_newsletter, ";
				$sql .= "contact_more_products, "; 
				$sql .= "contact_date, ";
				$sql .= "contact_time ";
				$sql .= ") VALUES (:contactName, :contactEmail, :contactReason, :contactComments, :contactNewsletter, :contactProductInfo, :contactDate, :contactTime)";
				
				//PREPARE the SQL statement
				$stmt = $conn->prepare($sql);
				
				//BIND the values to the input parameters of the prepared statement
				$stmt->bindParam(':contactName', $inName);
				$stmt->bindParam(':contactEmail', $inEmail);		
				$stmt->bindParam(':contactReason', $inContactReason);		
				$stmt->bindParam(':contactComments', $inComments);		
				$stmt->bindParam(':contactNewsletter', $inMailList);
				$stmt->bindParam(':contactProductInfo', $inProductInfo);				
				$stmt->bindParam(':contactDate', $formSubmitDate);	
				$stmt->bindParam(':contactTime', $formSubmitTime);	
				
				//EXECUTE the prepared statement
				$stmt->execute();	
			
				$conn = null;
				
				$message = "<span style='color:#337AB7;'>Thank you,".$inName."<br> We will be in contact with you shortly.</span>";            //success msg 
				$inName = $inEmail = $inContactReason = $inComments = $formSubmitDate = $formSubmitTime = $inMailList = $inProductInfo = "";   //clear user input after success
			}catch(PDOException $e){
				$message = "<span style='color:#b20000;'>There has been a problem. The system administrator has been contacted. Please try again later.</span>";   
			}
		}else{
			$message = "<span style='color:#b20000;'>Invalid entry. Please try again.</span>";    //if invalid form, displays error message if any user input is invalid
		}//ends check for valid form		
	}
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PHP Final Project - Furniture Co</title>
  <!-- Bootstrap core CSS -->
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
  <!--Text Styles Google Fonts-->
  <link href="https://fonts.googleapis.com/css?family=Comfortaa:300|Numans|Poppins:500i|Ubuntu:700i" rel="stylesheet">
  <!--Icons Font Awesome-->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!--Custom CSS--> 
  <link href="HomeworkPageFiles/FPstyles.css" rel="stylesheet">
  <link href="HomeworkPageFiles/FPcontactStyles.css" rel="stylesheet">
  <script src='HomeworkPageFiles/jquery-3.2.1.min.js'></script>
  <script>
		$(document).ready(function() {
		//on click of up arrow animated scroll to top
			$('#topButton').click(function() {
				$("html, body").animate({      //body is used by webkit browsers, html is used by firefox
					scrollTop:0 
				}, 1000)                      //speed
			});
		});
  </script>
</head>
<body>

	<nav class="navbar navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="FPindex.php"><img src="HomeworkPageImages/ChairLogo3.png" width="30" height="33" class="d-inline-block align-left"/> Furniture Co </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
			<li><a href="FPindex.php">Designs</a></li>
			<li><a href="#">About</a></li>
			<li><a href="#">Showroom</a></li>
			<li  class="active"><a href="FPcontact.php">Contact</a></li>
		  </ul>
		  <ul class="nav navbar-nav navbar-right">
			<li><a id="greeting"><?php echo $messageSuccess?></a></li>
			<li><a href="FPlogin.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
			<li><a href="FPlogout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
		  </ul>
        </div><!--/.navbar-collapse -->
      </div>
	 
    </nav>
	
    <!-- Main jumbotron-->
    <div class="jumbotron">
      <div class="container">
        <h1><img src="HomeworkPageImages/ChairLogo3.png" width="101" height="113"/>Furniture Co</h1>
        <p>As we evolve, our homes should, too.</p>
      </div>
	  <div class="d-block p-3 contactBlock">
		<div class="col-12">
			<h2>Contact Us</h2>
		</div>
	</div>
    </div>
	 

    <div class="container mainContent">
	  
      <div class="row">
			<h3 style="text-align:center;"><?php echo $message ;?></h3>
			<!--Form section-->		
			<form class="form-horizontal" id="contactForm" name="contactForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				
				<div class="form-group">
					<label class="col-sm-12 col-md-2 col-form-label">Name</label>
					<div class=" col-md-10">
						<span class="error"><?php echo $nameError; ?></span>
						<input type="text" name="contact_name" id="name" class="form-control" value="<?php echo $inName;?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-12 col-md-2 col-form-label">Email</label>
					<div class=" col-md-10">
						<span class="error"><?php echo $emailError; ?></span>
						<input type="text" name="contact_email" id="email" class="form-control" placeholder="youremail@google.com"  value="<?php echo $inEmail;?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-12 col-md-2 col-form-label">Contact Reason</label>
					<div class="col-md-10">
						<span class="error"><?php echo $contactReasonError; ?></span>
						<select name="contact_reason" id="contactReason" class="form-control">
							<option value="default" <?php if($inContactReason == "default") { echo "selected='selected'"; } ?>>( Please Select a Reason )</option>
							<option value="Custom-Product" <?php if($inContactReason == "Custom-Product") { echo "selected='selected'"; } ?>>Custom Furniture</option>
							<option value="Product-Inquiry" <?php if($inContactReason == "Product-Inquiry") { echo "selected='selected'"; } ?>>Product Inquiry</option>
							<option value="Product-Problem" <?php if($inContactReason == "Product-Problem") { echo "selected='selected'"; } ?>>Product Problem</option>
							<option value="Other" <?php if($inContactReason == "Other") { echo "selected='selected'"; } ?>>Other - If 'Other' please state reason in 'Comments' section.</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-12 col-md-2 col-form-label">Comments</label>
					<div class=" col-md-10">
						<span class="error"><?php echo $commentsError; ?></span>
						<textarea name="contact_comments" id="commentTextarea" rows="3" class="form-control" ><?php echo $inComments;?></textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 col-form-label"></label>
					<div class="col-sm-10 col-md-10">
						<div class="form-check">
							<label class="form-check-label">
								<input type="checkbox" name="contact_newsletter" id="mailListCheck" class="form-check-input" value="Yes"  <?php if($inMailList == 'Yes') {echo 'checked';} ?>> Please put me on your mailing list.
							</label>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 col-md-2 col-form-label"></label>
					<div class="col-sm-10 col-md-10">
						<div class="form-check">
							<label class="form-check-label">
								<input type="checkbox" name="contact_more_products" id="moreInfoCheck" class="form-check-input" value="Yes"  <?php if($inProductInfo == 'Yes') {echo 'checked';} ?>> Send me more information about your furniture.
							</label>
						</div>
					</div>
				</div>
				
				<div id="phony">
					<label>Phone:</label>
					<input type="text" name="Phone">
				</div>											
				
				<div class="form-group">
					<div class="col-12 text-center">
						<button type="submit" name="submit" value="Submit" id="submit">Submit</button>
						<button type="reset" name="reset" value="Reset" id="reset">Reset</button>
					</div>
				</div>
		
			</form><!--end form-->
      </div><!--end row-->

    </div> <!-- /container -->
	
		<p id="btn2"><a id="topButton" href="#">^</a></p>
	
		<div class="d-block p-3 contactBlock">
			<div class="col-12">
				<h6><strong><em>Get connected</em></strong>
					<!--Facebook-->
					<a><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
					<!--Twitter-->
					<a><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
					<!--Linkedin-->
					<a><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a>
					<!--Instagram-->
					<a><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
				</h6>
			</div>
		</div>
		
		<!--Footer-->
		<footer>
			<!--Footer Links-->
			<div class="container text-center">
				<div class="row">
					<!--First column-->
					<div class="col-md-3 col-lg-4 col-xl-3 ">
						<h6><a href="FPindex.php" id="brand"><strong>Furniture Co</strong></a></h6>
						<hr style="width: 60px;">
						<p class="slogan">As we evolve, <br>our homes should, too.</p>
					</div>
					<!--Second column-->
					<div class="col-md-2 col-lg-2 col-xl-2 ">
						<h6><strong>Useful Links</strong></h6>
						<hr style="width: 60px;">
						<p><a href="FPindex.php">Designs</a></p>
						<p><a href="#">Showroom</a></p>
						<p><a href="#">About</a></p>
						<p><a href="FPcontact.php">Contact</a></p>
					</div>
					<!--Third column-->
					<div class="col-md-3 col-lg-2 col-xl-2 ">
						<h6><strong>Showroom Hours</strong></h6>
						<hr style="width: 60px;">
						<p>Mon <em>closed</em></p>
						<p>Tues-Thur 9am - 6pm</p>
						<p>Fri 9am - 8pm</p>
						<p>Sat &amp; Sun 8am - 9pm</p>
					</div>
					<!--Fourth column-->
					<div class="col-md-4 col-lg-3 col-xl-3">
						<h6><strong>Contact</strong></h6>
						<hr style="width: 60px;">
						<p> New York, NY 10012, US</p>
						<p> info@example.com</p>
						<p> + 01 234 567 88</p>
						<p> + 01 234 567 89</p>
					</div>
				</div>
			</div>
			<!-- Copyright-->
			<div id="copyright">
				<div class="container-fluid">
					© 2017 Copyright: <a href="#"><strong> Furniture Co</strong></a>
				</div>
			</div>
		</footer>
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
   
</body>
</html>