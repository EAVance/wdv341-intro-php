<?php
	$inName = $inDescription = $inPresenter = $inDate = $inTime = $formSubmitDate = "";    
	$nameError = $descriptionError = $presenterError = $dateError = $timeError = "";   
	$validForm = false;

	//if form has been submitted gather user input and start validations
	if(isset($_POST["submit"])){         
		$inName = $_POST['event_name'];
		$inDescription = $_POST['event_description'];
		$inPresenter = $_POST['event_presenter']; 
		$inDate = $_POST['event_date'];
		$inTime = $_POST['event_time'];
		
		//validating name, if empty or just spaces - form is invalid & error message displays, else sanitize string
		function validateName(){           
			global $validForm, $nameError, $inName;		
			if(trim($inName) == ""){
				$validForm = false;
				$nameError = "Event Name is Required.";
			}else{
				$inName = filter_var($inName, FILTER_SANITIZE_STRING);
			}
		}
		
		//validating description, if empty or spaces - form is invalid & error message displays, else sanitize string
		function validateDescription(){
			global $validForm, $descriptionError, $inDescription;
			if(trim($inDescription) == ""){
				$validForm = false;
				$descriptionError = "Event Description is Required.";
			}else{
				$inDescription = filter_var($inDescription, FILTER_SANITIZE_STRING);
			}
		}
	
		//validating presenter, if empty or just spaces - form is invalid & error message displays, else sanitize string
		function validatePresenter(){           
			global $validForm, $presenterError, $inPresenter;		
			if(trim($inPresenter) == ""){
				$validForm = false;
				$presenterError = "Event Presenter is Required.";
			}else{
				$inPresenter = filter_var($inPresenter, FILTER_SANITIZE_STRING);
			}
		}
		
		//validate date, if blank - form is invalid & error message displays
		function validateDate(){
			global $validForm, $dateError, $inDate;
			if($inDate == ""){
				$validForm = false;
				$dateError = "Event Date is Required.";
			}
		}
		
		//validate time, if blank - form is invalid & error message displays
		function validateTime(){
			global $validForm, $timeError, $inTime;
			if($inTime == ""){
				$validForm = false;
				$timeError = "Event Time is Required.";
			}
		}
		
		
		function validatePhony(){
			global $validForm, $inPhone;
			if($inPhone){
				$validForm = false;
			}
		}
	
		$validForm = true;       
		//calling validation functions
		validateName(); 
		validateDescription();
		validatePresenter();
		validateDate();
		ValidateTime();
		validatePhony();
			
		if($validForm){
			$message = "All good";	
			
			try {
				require 'HomeworkPageFiles/connectPDO.php';	//CONNECT to the database		
				
				//Create the SQL command string
				$sql = "INSERT INTO wdv341_event (";    
				$sql .= "event_name, ";             
				$sql .= "event_description, ";
				$sql .= "event_presenter, ";
				$sql .= "event_date, ";
				$sql .= "event_time ";   
				$sql .= ") VALUES (:eventName, :eventDescription, :eventPresenter, :eventDate, :eventTime)";
				
				//PREPARE the SQL statement
				$stmt = $conn->prepare($sql);
				
				//BIND the values to the input parameters of the prepared statement
				$stmt->bindParam(':eventName', $inName);
				$stmt->bindParam(':eventDescription', $inDescription);		
				$stmt->bindParam(':eventPresenter', $inPresenter);		
				$stmt->bindParam(':eventDate', $inDate);		
				$stmt->bindParam(':eventTime', $inTime);	
				
				//EXECUTE the prepared statement
				$stmt->execute();	
				
				$message = "Your event has been registered and have successfully submitted the following information:";
			}catch(PDOException $e){
				$message = "There has been a problem. The system administrator has been contacted. Please try again later.";
			}
		}else{
			$message = "Something went wrong";
		}//ends check for valid form	
	}
;?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:800i|Montserrat" rel="stylesheet">
	<link rel="stylesheet" href="HomeworkPageFiles/jquery-ui-dot-lov-theme/jquery-ui.css">
	<link rel="stylesheet" href="HomeworkPageFiles/jquery-timepicker-1.3.5/jquery.timepicker.min.css">
	<!--JS-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="HomeworkPageFiles/jquery-3.2.1.min.js"></script>
	<script src="HomeworkPageFiles/jquery-ui-dot-lov-theme/jquery-ui.js"></script>
	<script src="HomeworkPageFiles/jquery-timepicker-1.3.5/jquery.timepicker.min.js"></script>
	<style>
		body{
			background: #0b3e6f;
			font-family: 'Montserrat', sans-serif;
			letter-spacing:0.05em;}
		h2{
			color:#d9d9d9;
			text-align:center;
			margin-top:4em;
			font-family: 'Merriweather Sans', sans-serif;}
		h3{
			color:#848484;
			text-align:center;
			font-family: 'Merriweather Sans', sans-serif;
			margin-bottom:0;}
		form{
			width: 375px;
			margin:0 auto;
			border-radius: 5px;
			padding-top:2em;
			padding-bottom:2em;
			border-left:none;
			border-right:none;
			border-top:2px solid #505050;
			border-bottom:2px solid #505050;
			background:rgba(17,17,17,.2);}
		label{
			font-weight:bold;
			color:#000000;}
		input[type="text"] {
			color:#0D0D0D;
			border-radius:4px;
			border-top:none;
			border-bottom:none;
			border-right:none;
			border-left:2px solid #505050;
			background-color:rgba(99,99,99,.08);
			font-size:1.2em;
			padding:.4em .5em .4em 1em;
			width: 16.5em;
			margin-top:2px;}
		fieldset {
			border: 0;
			padding-bottom:1em;}
		.formButtons {
			text-align:center;}
		input[type="reset"],input[type="submit"],button {
			margin:.7em;
			padding:.5em .7em;
			border-radius: 4px;
			font-size:1.1em;
			color:#000000;
			border-top:none;
			border-bottom:none;
			border-left:2px solid #505050;
			border-right:2px solid #505050;
			box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);
			background:rgba(11,62,111,.6);
			letter-spacing:0.05em;}
		input[type="reset"]:hover,input[type="submit"]:hover,button:hover {
			color:#d9d9d9;
			box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.4);}
		.ui-datepicker{
			width:16em;}
		.ui-tooltip{
			box-shadow:none;}
		.ui-timepicker{
			background:#111111;}
		.error {
			color:#d75838;
			font-style:italic;}
		#phony {
			display: none;}
		#custInput {
			font-family: 'Montserrat', sans-serif;
			font-size:1.3em;
			letter-spacing:0.07em;
			margin-bottom:2em;}
		#custInput ul{
			list-style-type:none;
			padding-top:2em;
			padding-left:0px;}
		#custInput li {
			padding-top:4px;}
		#submitSuccess {
			width: 375px;
			margin:2em auto;
			border-radius: 5px;
			padding:2em .6em;
			border-left:none;
			border-right:none;
			border-top:2px solid #505050;
			border-bottom:2px solid #505050;
			background:rgba(17,17,17,.2);}
		.successMsg{
			padding-top:1em;}
	</style>
	<script>
		$(document).ready(function(){
			$( function() {
				$( "#datepicker" ).datepicker({      
					showAnim: "slide",               //datepicker show animation turned to slide in and out
					changeMonth: true,               //allows user to pick month in drop down
					changeYear: true,                //allows user to pick year in drop down
					minDate: 0,                      //restricts the datepicker range by not allowing dates before today to be picked
					altField: "#actualDate",         //makes alternate input date field to send to database
					altFormat: "yy-mm-dd",           //alternate date format for alternate input date field
				});
				
			} );
			$( function() {
				$( document ).tooltip();
			} );
			$('input.timepicker').timepicker({       //http://timepicker.co/	
				timeFormat: 'h:mm:ss p',             //time format 00:00:00 PM/AM
				scrollbar:true, 
			});     
		});
	</script>
</head>
<body>
	<!--Main section-->
	<div class="container">
	  
	<?php      
		if ($validForm) {	   //if valid form remove form and display thank you message & submitted info	
			
	?>
	
			<div id="submitSuccess">
				<div class="successMsg">
					<h3>Thank you, <br><?php echo $message ?></h3> 
				</div>
				<div id="custInput">
					<ul>
						<li><strong>Event Name:</strong> <em><?php echo $inName;?></em></li>
						<li><strong>Event Description:</strong> <em><?php echo $inDescription;?></em></li>
						<li><strong>Event Presenter:</strong> <em><?php echo $inPresenter;?></em></li>
						<li><strong>Event Date:</strong> <em><?php echo $inDate;?></em></li>
						<li><strong>Event Time:</strong> <em><?php echo $inTime;?></em></li>
					</ul>
				</div>
				<h3>Register Another Event</h3>
				<p class="formButtons">
					<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/eventsForm.php'">Register Event</button>
					<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/6350d82e769afa7844f5aa8423e6debe03bed7f9/eventsForm.php?at=master&fileviewer=file-view-default'">View PHP</button>
				</p>
			</div>
		
	<?php	 		
		}else {      //if not valid form display form again with previously entered event info
	?>
			
			<h2>Event Registration</h2>
	 
			<!--Form section-->		
			<form  id="eventsForm" name="eventsForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				
				<fieldset>
					<label>Event Name <br><span class="error"><?php echo $nameError; ?></span></label>
						<input type="text" name="event_name" id="eventName" size="25" value="<?php echo $inName;?>">
				</fieldset>
				<fieldset>
					<label>Event Description <br><span class="error"><?php echo $descriptionError; ?></span></label>
						<input type="text" name="event_description" id="description" size="25" value="<?php echo $inDescription;?>">
				</fieldset>
				<fieldset>
					<label>Event Presenter <br><span class="error"><?php echo $presenterError; ?></span></label>
						<input type="text" name="event_presenter" id="presenter" size="25" value="<?php echo $inPresenter;?>">						
				</fieldset>
				<fieldset>
					<label for="date">Event Date <br><span class="error"><?php echo $dateError; ?></span></label>
						<input type="text" id="datepicker" readonly="readonly" size="25" value="<?php echo $inDate;?>">	
						<input type="hidden" name="event_date" id="actualDate">							
				</fieldset>
				<fieldset>
					<label>Event Time <br><span class="error"><?php echo $timeError; ?></span></label>
						<input type="text" name="event_time" id="time" class="timepicker" size="25" value="<?php echo $inTime;?>">	
				</fieldset>
				<fieldset>
					<div id="phony">
						<label>Phone:</label>
						<input type="text" name="Phone">
					</div>											
				</fieldset>
				<div class="formButtons">
					<input type="reset" name="reset" value="Reset" id="reset">
					<input type="submit" name="submit" value="Submit" id="submit">
				</div>
		
			</form><!--end form-->
	  
	<?php
		}      // end else 
	?>
	  
	</div><!--end main container -->

</body>
</html>