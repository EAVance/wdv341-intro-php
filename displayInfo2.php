<?php
	require 'HomeworkPageFiles/connectPDO.php';	
	
	try {
	  //select for table data
		$sql = $conn->prepare('SELECT cust_email, cust_pref1, cust_pref2, cust_pref3, cust_pref4 FROM time_preferences');
		$sql->execute();
		$count = $sql->rowCount();  
	  //select for sum of each column of time preferences
		$sql2 = $conn->prepare('SELECT SUM(cust_pref1) AS pref1Total, SUM(cust_pref2) AS pref2Total, SUM(cust_pref3) AS pref3Total, SUM(cust_pref4) AS pref4Total FROM time_preferences');
		$sql2->execute();
		$colTotal = $sql2->fetch(); 
		
	  //finds lowest value in returned data from $sql2, finds which column total it matches, then displays corresponding message
		$firstPref = min($colTotal);
		$firstPrefInd = array_search($firstPref, $colTotal);  //search array for the value of $firstPref and return its key
		$firstPrefMsg;
		if($firstPrefInd == 'pref1Total'){
			$firstPrefMsg = "Mon/Wed 10:10am - Noon is the preferred time.";
		}else if($firstPrefInd == 'pref2Total'){
			$firstPrefMsg = "Tuesday 6:00pm - 9:00pm is the preferred time.";
		}else if($firstPrefInd == 'pref3Total'){
			$firstPrefMsg = "Wednesday 6:00pm - 9:00pm is the preferred time.";
		}else{
			$firstPrefMsg = "Tue/Thur 10:10am - Noon is the preferred time.";
		}
		
	  //displays data from $sql in table 
		if($count > 0){
			$displayMsg = "<table><tr class='tableHeader'><td>Email</td><td>Mon/Wed <br>10:10am - Noon</td><td>Tuesday <br>6:00-9:00pm</td><td>Wednesday <br>6:00-9:00pm</td><td>Tue/Thur <br>10:10am - Noon</td></tr>";
			while($row = $sql->fetch()) {
				$displayMsg .= "<tr><td>".$row["cust_email"]."</td>";
				$displayMsg .= "<td>".$row["cust_pref1"]."</td>";
				$displayMsg .= "<td>".$row["cust_pref2"]."</td>";
				$displayMsg .= "<td>".$row["cust_pref3"]."</td>";
				$displayMsg .= "<td>".$row["cust_pref4"]."</td></tr>";
			}
		   //displays column totals from $sql2 in last row
			$displayMsg .= "<tr class='totalRow'><td>Total</td><td>".$colTotal['pref1Total']."</td><td>".$colTotal['pref2Total']."</td><td>".$colTotal['pref3Total']."</td><td>".$colTotal['pref4Total']."</td></tr></table>";
		}else {
			$displayErrorMsg = "<h3><em>Zero results were found</em></h3>";			
		}	
	}catch(PDOException $e){
		$displayErrorMsg = "<h3><em>Sorry there has been a problem.</em><br>" . $e->getMessage()."</h3>";
	}
	
	$conn = null;
	
;?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:800i|Montserrat" rel="stylesheet">
	<style>
		body{
			background: #0b3e6f;
			color:#000000;
			font-size:1.1em;
			font-family: 'Montserrat', sans-serif;
			letter-spacing:1.2px;}
		#content{
			width:90%;
			margin:0 auto;}
		h3{
			color:#d9d9d9;
			text-align:center;
			margin-top:4em;
			font-family: 'Merriweather Sans', sans-serif;}
		.tableHeader{
			text-align:center;
			font-size:1.2em;
			font-weight:bold;}
		table{
			width:90%;
			margin:2em auto;
			font-size:.9em;
			border-collapse: collapse;}
		tr + tr { 
			border-top: 1px solid rgba(152,152,152,.3); }
		td{
			padding:4px;}
		td + td {
			border-left:1px solid rgba(152,152,152,.3);
			padding:.6em;}
		td:not(:first-child) {
			text-align:center;}
		.formButtons {
			text-align:center;}
		.totalRow{
			text-align:center;
			font-weight:bold;}
		button{
			margin:.8em;
			padding:.3em .5em;
			border-radius: 4px;
			font-size:1.1em;
			color:#000000;
			text-decoration:none;
			border-top:none;
			border-bottom:none;
			border-left:2px solid #505050;
			border-right:2px solid #505050;
			box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);
			background-color:rgba(17,17,17,.2);}
		button:hover{
			color:#d9d9d9;
			background-color:rgba(11,62,111,.6);
			box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.4);}
		@media only screen and (max-width: 1200px){
			#content{
				width:100%;}
		}
	</style>
</head>
<body>

<?php      
	if($count > 0) {	   //if query results have more than 0 records in database this will show	
?>

	<h3>Time Preferences</h3>
	<div id="content">
		<?php echo $displayMsg; ?>
		<h3><?php echo $firstPrefMsg; ?></h3>
	</div>
	
<?php	 		
	}else {     //if the query results have 0 records in database or error, this message will show
?>

	<div>
		<?php echo $displayErrorMsg; ?>
	</div>
	
<?php
	}      // end else 
?>
	<p><a href='https://bitbucket.org/EAVance/wdv341/src/d0ae0efaf54bcb1fd0f522d4fe56d42d7fc8af04/displayInfo2.php?at=master&fileviewer=file-view-default'>View PHP</a></p>
</body>
</html>