<?php
	require 'HomeworkPageFiles/connectPDO.php';	
	$displayMsg = $displayErrorMsg = "";
	
	try {
		$result = $conn->prepare('SELECT event_name, event_description, event_presenter, event_date, event_time, event_id FROM wdv341_event');
		$result->execute();
		if($result->rowCount() > 0){
			$displayMsg .= "<table><tr class='tableHeader'><td>Name</td><td>Description</td><td>Presenter</td><td>Date</td><td>Time</td></tr>";
			while($row = $result->fetch()) {
				$displayMsg .= "<tr><td>".$row['event_name']."</td>";
				$displayMsg .= "<td>".$row["event_description"]."</td>";
				$displayMsg .= "<td>".$row["event_presenter"]."</td>";
				$displayMsg .= "<td>".$row["event_date"]."</td>";
				$displayMsg .= "<td>".$row["event_time"]."</td>";
				$displayMsg .= "<td><a href='deleteEvent.php?event_id=".$row['event_id']."' class='tooltip'><i class='fa fa-trash-o fa-lg' aria-hidden='true'></i> <span class='tooltiptext'>Delete</span></a></td></tr>"; 
			}
			$displayMsg .= "</table>";
		}else {
			$displayErrorMsg = "<h3><em>Zero results were found</em></h3>";			
		}	
	}catch(PDOException $e){
		$displayErrorMsg = "<h3><em>Sorry there has been a problem.</em><br>" . $e->getMessage()."</h3>";
	}
	$conn = null;
	
;?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:800i|Montserrat" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="HomeworkPageFiles/selectEventsStyles.css">
	<script src="HomeworkPageFiles/jquery-3.2.1.min.js"></script>
</head>
<body>

<?php      
	if ($result->rowCount() > 0) {	   //if query results have more than 0 records in database the events will show	
?>

	<h3>Events That Have Been Registered</h3>
	<div id="content">
		<?php echo $displayMsg; ?>
	</div>
	
<?php	 		
	}else {   //if the query results are 0 records in database or error, message will show
?>

	<div>
		<?php echo $displayErrorMsg; ?>
	</div>
	
<?php
	}      // end else 
?>
	
	<div class="formButtons">
		<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/c5c90fb3f06af283a02e3cc2f61601b2bef3f172/selectEvents2.php?at=master&fileviewer=file-view-default'">View PHP</button>
	</div>
	
</body>
</html>