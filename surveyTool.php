<?php
	$inEmail = $formSubmitDate = $inMonWedPref = $inTuesPref = $inWedPref = $inTuesThurPref = $inPhone = "";    
	$emailError = $timePrefError = "";   
	$validForm = false;

	//if form has been submitted gather user input and start validations
	if(isset($_POST["submit"])){         
		
		$inEmail = $_POST['cust_email'];
		$inMonWedPref = $_POST['cust_pref1'];
		$inTuesPref = $_POST['cust_pref2'];
		$inWedPref = $_POST['cust_pref3'];
		$inTuesThurPref = $_POST['cust_pref4'];
		$inPhone = $_POST['Phone'];
		 
		
		//validating email, if empty or spaces OR not a valid email - form is invalid & error message displays
		function validateEmail(){
			global $validForm, $emailError, $inEmail;
			if(trim($inEmail) == ""){
				$validForm = false;
				$emailError = "Email is Required.";
			}else if(!preg_match("/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/",trim($inEmail))){        
				$validForm = false;
				$emailError = "Invalid Email (eg. yourName@yahoo.com)";
			}
		}
		
		//validate time preferences, if one is not selected then invalid form and error message displays
		function validateTimePref(){
			global $validForm, $timePrefError,$inMonWedPref, $inTuesPref, $inWedPref, $inTuesThurPref;
			if(!isset($timePrefError,$inMonWedPref,$inTuesPref,$inWedPref,$inTuesThurPref)){
				$validForm = false;
				$timePrefError = "Please Rate All Time Slots";
			}
		}
		
		function validatePhony(){
			global $validForm, $inPhone;
			if($inPhone){
				$validForm = false;
			}
		}
	
		//date of form submission
		function formSubmitDate(){
			global $formSubmitDate;
			date_default_timezone_set('America/Chicago');
			$formSubmitDate = date('m/d/y \a\t h:i A');
		}
		
		$validForm = true;       
		//calling validation functions
		validateEmail();
		validateTimePref();
		validatePhony();
		formSubmitDate();	

		if($validForm){
			try {
				require 'HomeworkPageFiles/connectPDO.php';	//CONNECT to the database		
				
				//Create the SQL command string
				$sql = "INSERT INTO time_preferences(";    
				$sql .= "cust_email, ";             
				$sql .= "cust_pref1, ";
				$sql .= "cust_pref2, ";
				$sql .= "cust_pref3, ";
				$sql .= "cust_pref4, ";
				$sql .= "cust_input_date "; 
				$sql .= ") VALUES (:custEmail, :custPref1, :custPref2, :custPref3, :custPref4, :custInputDate)";
				
				//PREPARE the SQL statement
				$stmt = $conn->prepare($sql);
				
				//BIND the values to the input parameters of the prepared statement
				$stmt->bindParam(':custEmail', $inEmail);
				$stmt->bindParam(':custPref1', $inMonWedPref);		
				$stmt->bindParam(':custPref2', $inTuesPref);		
				$stmt->bindParam(':custPref3', $inWedPref);		
				$stmt->bindParam(':custPref4', $inTuesThurPref);
				$stmt->bindParam(':custInputDate', $formSubmitDate);				
				
				//EXECUTE the prepared statement
				$stmt->execute();	
				
			}catch(PDOException $e){
				$message = "There has been a problem. The system administrator has been contacted. Please try again later.";
			}
		}else{
			$message = "Invalid entry. Please try again.";
		}//ends check for valid form	
	}
;?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--Text Styles Google Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:600i|Questrial|Ubuntu:700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Expletus+Sans:700|Happy+Monkey" rel="stylesheet">
	<style>
		h1{
			text-align:center;}
		h2{
			color:#00004C;}
		form{
			width:425px;
			margin:0 auto;
			border:thin solid black;
			border-radius:4px;
			padding:3%;}
		.error {
			color:#b20000;
			font-weight:bold;
			font-style:italic;}
		#phony {
			display: none;}
		input[type=radio]{
			margin-left:2%;}
		input[type=text]{
			border:thin solid black;
			padding:1.2%;
			border-radius:4px;}
		span{
			color:#cc8300;}
		.surveyBtns{
			padding-top:5%;
			text-align:center;}
		button{
			background-color:transparent;
			border:thin solid black;
			padding:1.2%;
			margin:2%;
			font-size:1.2em;
			border-radius:4px;}
		button:hover{
			background-color:black;
			color:#ffffff;}
		#custInput ul{
			list-style-type:none;
			color:#cc8300;
			font-size:1.2em;
			width:400px;
			margin:0 auto;
			text-align:left;}
		#submitSuccess{
			text-align:center;
			color:#00004C;
			margin-top:5%;}
	</style>
	<!--JS-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>

	<!--Main section-->
	<div class="container">
		
	  
	<?php      
		if ($validForm) {	   //if valid form remove form and display thank you message & submitted info	
			
	?>
			<div id="submitSuccess">
				<div class="successMsg">
					<h3>Thank you,</h3> 
					<h3>You have successfully submitted the following information:</h3>
				</div>
				<div id="custInput">
					<ul>
						<li><strong>Email:</strong> <em><?php echo $inEmail;?></em></li>
						<li><strong>Monday/Wednesday 10:10am - Noon: </strong> <em><?php echo $inMonWedPref;?></em></li>
						<li><strong>Tuesday 6:00-9:00pm: </strong> <em><?php echo $inTuesPref;?></em></li>
						<li><strong>Wednesday 6:00-9:00pm: </strong> <em><?php echo $inWedPref;?></em></li>
						<li><strong>Tuesday/Thursday 10:10am - Noon: </strong> <em><?php echo $inTuesThurPref;?></em></li>
					</ul>
				</div>
			</div>
		<div class="surveyBtns">
			<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/surveyTool.php'">Back</button>
			<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/ec479f2a2f58b4150e99a9b6f177484ec1164c8e/surveyTool.php?at=master&fileviewer=file-view-default'">View PHP</button>
		</div>
	<?php	 		
		}else {      //if not valid form display form again with previously entered cust info
	?>
			
			<h1>Survey</h1>
			<h2 class="error" style="text-align:center;"><?php echo $message ;?></h2>
			
			<!--Form section-->		
			<form id="contactForm" name="contactForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				
				<h2>Email <br><span class="error"><?php echo $emailError; ?></span></h2>
				<input type="text" name="cust_email" id="email" placeholder="youremail@google.com"  value="<?php echo $inEmail;?>">	
			
				<h2>Please Rate The Following Times <br><span>(1 best - 4 worst)<br></span><span class="error"><?php echo $timePrefError; ?></span></h2>
				<h3>Monday/Wednesday 10:10am - Noon</h3>
				<span>Best </span><input type="radio" name="cust_pref1" value="1" <?php if ($inMonWedPref == '1') { echo "checked"; } ?>/> 1
				<input type="radio" name="cust_pref1" value="2" <?php if ($inMonWedPref == '2') { echo "checked"; } ?>/> 2
				<input type="radio" name="cust_pref1" value="3" <?php if ($inMonWedPref == '3') { echo "checked"; } ?>/> 3 
				<input type="radio" name="cust_pref1" value="4" <?php if ($inMonWedPref == '4') { echo "checked"; } ?>/> 4 <span> Worst</span>
				
				<h3>Tuesday 6:00-9:00pm</h3>
				<span>Best </span><input type="radio" name="cust_pref2" value="1" <?php if ($inTuesPref == '1') { echo "checked"; } ?>/> 1
				<input type="radio" name="cust_pref2" value="2" <?php if ($inTuesPref == '2') { echo "checked"; } ?>/> 2
				<input type="radio" name="cust_pref2" value="3" <?php if ($inTuesPref == '3') { echo "checked"; } ?>/> 3 
				<input type="radio" name="cust_pref2" value="4" <?php if ($inTuesPref == '4') { echo "checked"; } ?>/> 4 <span> Worst</span>
				
				<h3>Wednesday 6:00-9:00pm</h3>
				<span>Best </span><input type="radio" name="cust_pref3" value="1" <?php if ($inWedPref == '1') { echo "checked"; } ?>/> 1
				<input type="radio" name="cust_pref3" value="2" <?php if ($inWedPref == '2') { echo "checked"; } ?>/> 2
				<input type="radio" name="cust_pref3" value="3" <?php if ($inWedPref == '3') { echo "checked"; } ?>/> 3 
				<input type="radio" name="cust_pref3" value="4" <?php if ($inWedPref == '4') { echo "checked"; } ?>/> 4 <span> Worst</span>
				
				<h3>Tuesday/Thursday 10:10am - Noon</h3>
				<span>Best </span><input type="radio" name="cust_pref4" value="1" <?php if ($inTuesThurPref == '1') { echo "checked"; } ?>/> 1
				<input type="radio" name="cust_pref4" value="2" <?php if ($inTuesThurPref == '2') { echo "checked"; } ?>/> 2
				<input type="radio" name="cust_pref4" value="3" <?php if ($inTuesThurPref == '3') { echo "checked"; } ?>/> 3 
				<input type="radio" name="cust_pref4" value="4" <?php if ($inTuesThurPref == '4') { echo "checked"; } ?>/> 4 <span> Worst</span>
				
				<div id="phony">
					<label>Phone:</label>
					<input type="text" name="Phone">
				</div>											
				
				<div class="surveyBtns">
					<button type="reset" name="reset" value="Reset" id="reset">Reset</button>
					<button type="submit" name="submit" value="Submit" id="submit">Submit</button>
				</div>
		
			</form><!--end form-->
	  
	<?php
		}      // end else 
	?>
	
	</div><!--end main container -->

</body>
</html>