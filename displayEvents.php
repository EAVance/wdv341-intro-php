<?php
	require 'HomeworkPageFiles/connectPDO.php';
	$displayErrorMsg = "";
	
	try {
		$sql = $conn->prepare("SELECT event_name, event_description, event_presenter, DATE_FORMAT(event_day, '%m-%d-%Y') AS event_day, TIME_FORMAT(event_time, '%h:%i %p') AS event_time FROM wdv341_event2");
		$sql->execute();
		$count = $sql->rowCount(); 
		
	}catch(PDOException $e){
		$displayErrorMsg = "<h3><em>Sorry there has been a problem.</em><br>" . $e->getMessage()."</h3>";
	}
;?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>WDV341 Intro PHP  - Display Events Example</title>
    <style>
		.eventBlock{
			width:500px;
			margin-left:auto;
			margin-right:auto;
			background-color:#E7F3FD;	
			border-radius:2px;
			padding:2%;
			font-size:1.2em;}
		h2,h3{
			text-align:center;}
		p a{
			font-size:1.3em;
			color:black;
			text-decoration:none;}
		a:hover{
			font-style:italic;}
	</style>
</head>

<body>
	<p><a href='https://bitbucket.org/EAVance/wdv341/src/3e577eb61d1a35bfdc697b63da243a049755b8bc/displayEvents.php?at=master&fileviewer=file-view-default'>View PHP</a></p>
	<h2>WDV341 Intro PHP</h2>
	<h3>Display Events as formatted output blocks</h3>   
	<h3> <?php echo $count; ?> Events are available today.</h3>

<?php
	//Display each row as formatted output
	while( $row = $sql->fetch()){
		$displayEventName = $row["event_name"];
		$displayEventDescription = $row["event_description"];
		$displayEventPresenter = $row["event_presenter"];
		$displayEventDay = ($row["event_day"]);
		$displayEventTime = $row["event_time"]; 
?>

	<p>
        <div class="eventBlock">
			<div>
				<span class="displayEvent">Event: <?php echo $displayEventName; ?></span>
			</div>
			<div>
				<span class="displayDescription">Description: <?php echo $displayEventDescription; ?></span>
			</div>
			<div>
				<span class="displayPresenter">Presenter: <?php echo $displayEventPresenter; ?></span>
			</div>
			<div>
				<span class="displayTime">Day: <?php echo $displayEventDay; ?></span>
			</div>
			<div>
				<span class="displayDate">Time: <?php echo $displayEventTime; ?></span>
			</div>
		</div>
    </p>

	<div>
		<?php echo $displayErrorMsg; ?>
	</div>

<?php
  	}//close while loop
	$conn = null;
?>

</body>
</html>