<?php 
	$inMonth = $_POST["Month"];		
	$inDay = $_POST["Day"];		
	$inYear = $_POST["Year"];
	$inRandomText = $_POST["RandomText"];
	$inNumFormat = $_POST["NumberFormat"];
	$inCurrencyFormat = $_POST["CurrencyFormat"];

	function formatUSdate($month,$day,$year){
		$USdate = date("m/d/Y", mktime(0, 0, 0, $month, $day, $year));
		return $USdate;
	}
	function formatEUROdate($month,$day,$year){
		$EUROdate = date("d/m/Y", mktime(0, 0, 0, $month, $day, $year));
		return $EUROdate;
	}
	function randomText($randomText){
		$findSubStr = "dmacc";
		$convertedText = strtolower(trim($randomText));
		$pos = strpos($convertedText,$findSubStr);
		if($pos === false){
			$result = "<em>No</em>";
		}else{
			$result = "<em>Yes</em>";
		}
		return ("<li>Converted to no leading or trailing white spaces & turned to all lowercase:<br><em>" .$convertedText. "</em></li><li>Character count:  <em>". strlen($convertedText) ."</em></li><li>DMACC substring found:  ". $result. "</li>");
	}
	function formatNumbers($numbers){
		$formattedNum = number_format($numbers);
		return $formattedNum;
	}
	function formatCurrency($numbers){
		$formattedNum = "$ ". number_format($numbers, 2);
		return $formattedNum;
	}
;?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>WDV 341 PHP Functions Results</title>
	<style>
		#container{
			width:375px;
			margin:0 auto;
			color:#333333;
			font-family:Gill Sans, Arial, sans-serif;
			font-size:1.1em;
			border: 1px solid #93c3cd;
			padding:1.5em 1em;
			border-radius: 4px;
		}
		button {
			margin-top:1em;
			border-radius: 4px;
			color:#333333;
			border:1px solid #93c3cd;
			background:rgba(255,255,255, 0.8);
			box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);
			padding:.2em .4em;
			margin:1em .5em;
			font-size:1.05em;
		}
		button:hover{
			background:#FFFFFF;
			box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.4);
		}
		#formButtons{
			text-align:center;
		}
		li,#returnStyles {
			color:#93c3cd;
		}
		#returnStyles {
			font-style:italic;
		}
	</style>
</head>

<body>
  <div id="container">
	<h2></h2>

		<p>Input date '<em><?php echo $inMonth.", ".$inDay.", ".$inYear;?></em>' formatted to U.S. mm/dd/yyyy :<br><span id="returnStyles"><?php echo formatUSdate($inMonth,$inDay,$inYear); ?></span></p>
		<p>Input date '<em><?php echo $inMonth.", ".$inDay.", ".$inYear;?></em>' formatted to EURO dd/mm/yyyy :<br><span id="returnStyles"><?php echo formatEUROdate($inMonth,$inDay,$inYear); ?></span></p>
		<p>Input random text <?php echo "'<em>".$inRandomText."</em>' :<br>" . randomText($inRandomText); ?></p>
		<p>Input numbers '<em><?php echo $inNumFormat;?></em>' formatted: <br><span id="returnStyles"><?php echo formatNumbers($inNumFormat); ?></span></p>
		<p>Input numbers '<em><?php echo $inCurrencyFormat;?></em>' formatted to U.S.currency: <br><span id="returnStyles"><?php echo formatCurrency($inCurrencyFormat); ?></span></p>
	
	<div id="formButtons">
		<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/phpFunctions.html'">Go Back</button>
		<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/09db8ea2355740464605ef359ab200e53ec5dbce/formHandler.php?at=master&fileviewer=file-view-default'">View PHP</button>
	</div>
	
  </div>
</body>
</html>