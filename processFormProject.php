<?php
	$inCustName = $_POST["Name"];		
	$inCustEmail = $_POST["Email"];	
	$inContactReason = $_POST["Contact-Reason"];	
	$inCustComments = $_POST["Comments"];
	$inMailListCheck = $_POST["Mail-List-Check"];
	$inMoreInfoCheck = $_POST["More-Info-Check"];

  //Email set up for 'user/customer' confirmation email
	$toCustEmail = $inCustEmail;				
	$subject = "NexGen Astronomer";
	$fromEmail = "contact@erinavance.info";		
	$custEmailBody = "
		<html>
		<head>
			<link href='https://fonts.googleapis.com/css?family=Expletus+Sans:700|Happy+Monkey' rel='stylesheet'>
			<style>
				body{
					background-color:#344141;}
				.main {
					background-color:#E8F0EE;
					color:#1A292E;
					margin:.1em;
					padding:.5em 2em;
					box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);}
				.header {
					margin:.1em;
					position: relative;
					text-align: center;
					text-shadow: 1px 2px 3px #323232; 
					color:#E8F0EE;
					font-family: 'Expletus Sans', cursive;}
				.centered {
					position: absolute;
					top: 50%;
					left: 50%;
					transform: translate(-50%, -50%);}
			</style>
		</head>
		<body>
		  <div class='header'>
				<div class='centered'><h2>NexGen Astronomer</h2></div>
				<img src='http://erinavance.info/courses/WDV341/WDV341Homework/HomeworkPageImages/spaceEmail.jpg' style='width:100%; margin:0; padding:0;'>
		  </div>
		  <div class='main'>
			<h3>Dear <em style='color:#7c6c5d;'>".$inCustName."</em>,</h3>
			<h4>Thank you for your comments or questions.<br> We will review your input and be in contact with you shortly.</h4>
			<h4><em style='color:#7c6c5d;'>NexGen Astronomer <br>Customer Service</em></h4>
		  </div>
		</body>
		</html>";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "From: $fromEmail" . "\r\n";			
	
  //Email is compiled & sent to 'user/customer' with above parts, and if email was accepted for delivery a 'successfully sent' confirmation message will display on browser window, else a 'message did not deliver' will display on browser window
	$emailSentMsg = "";
 	if (mail($toCustEmail,$subject,$custEmailBody,$headers)) {
   		$emailSentMsg = "<h3>Thank you <em style='color:#7c6c5d;'>".$inCustName."</em>,<br><br>Your message was successfully sent!<br>We will be sending a confirmation email to <em style='color:#7c6c5d;'>".$inCustEmail."</em> shortly!</h3>";
  	} else {
   		$emailSentMsg = "<h3><em>Sorry</em> something went wrong, your message did not deliver ...</h3>";
  	}
	
  //Email set up with name value pairs(customer information) sent to 'website owner/business'
	$custInput = "<h3 style='color:#344141;'>NexGen Astronomer Customer Information:</h3><br>";
	foreach($_POST as $key => $value) {
		$custInput.= "<div style='border: 1px solid #344141;border-radius: 4px;padding:5px;margin:5px;color:#8E8A87;width:400px;'>".$key."  :  ".$value."</div>\n";
	}
	mail("ealippert@dmacc.edu",$subject,$custInput,$headers);
	
;?>
<!DOCTYPE html>
<html>
  <head>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:600i|Questrial|Ubuntu:700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Expletus+Sans:700|Happy+Monkey" rel="stylesheet">
	<style>
		html{
			background-color:#344141;}		
		header {
			margin:2% 2% 0 2%;
			background-image:url("HomeworkPageImages/spaceEmail.jpg");
			background-size: cover;
			padding-top: 70px;
			padding-bottom: 60px; 
			box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);}	
		main {
			margin:0 2% 2% 2%;
			background:#E8F0EE;
			color:#1A292E;
			box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);
			padding:3em 1em 2em 1em;}	
		h1 {
			text-shadow: 1px 2px 3px #323232; 
			color:#E8F0EE;
			font-family: 'Expletus Sans', cursive;
			font-size:2.7em;}	
		h2 {
			font-weight: bold;
			letter-spacing: .07em;
			text-shadow: 1px 2px 3px #323232; 
			color:#E8F0EE;
			font-size:1.5em;
			font-family: 'Happy Monkey', cursive;}
		h1, h2 {
			text-align: center; 
			padding:0 2%; }
		h3{
			font-family: 'Questrial', sans-serif;
			font-size: 1.3em;
			text-align:center;
			margin:3em 0em;
			letter-spacing: .06em;
			line-height:1.2em}
		#formButtons {
			text-align:center;
			margin-top:2em;}
		button {
			margin:6% 1%;
			padding:.4em;
			font-family: 'Questrial', sans-serif;
			font-size: 1.25em;
			letter-spacing: 0.05em;
			font-weight: bold;
			border: medium solid #344141;
			background-color: transparent;
			transition: .4s ease-in-out; }
		button:hover {
			color: #ffffff;
			font-weight: initial;
			background-color: rgba(52,65,65,0.4); }
	</style>
  </head>
  <body>
  
	<header>
		<h1>NexGen Astronomer</h1>
		<h2>Inspiring the next generation of explorers</h2>
	</header>
	
	<main>
		<?php echo $emailSentMsg ;?>
		<div id="formButtons">
			<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/contactFormEmailProject.html'">Back to Contact Form</button>
			<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/36ff4250b81b4d7d28a94ab8412e31250b83790c/processFormProject.php?at=master&fileviewer=file-view-default'">View PHP</button>
		</div>
	</main>
	
  </body>
</html>