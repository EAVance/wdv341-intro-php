<?php
	class Email {
		private $recipient;     //send to email address   
		private $sender;        //from email address
		private $subject;       //content for subject line
		private $message;       //email body content
		
		//contructor method goes here 
		//public function __construct($inRecipient){
		//$this->setRecipient($inRecipient);             
		//}
		
		public function setRecipient($inRecipient){     
			$this->recipient = $inRecipient;            
		}
		public function getRecipient(){
			return $this->recipient;                    
		}
		
		public function setSender($inSender){
			$this->sender = $inSender;
		}
		public function getSender(){
			return $this->sender;
		}
		
		public function setSubject($inSubject){
			$this->subject = $inSubject;
		}
		public function getSubject(){
			return $this->subject;
		}
		
		public function setMessage($inMessage){
			$this->message = $inMessage;
		}
		public function getMessage(){
			return $this->message;
		}
		
		//other methods
		public function sendMail(){
			$to = $this->getRecipient();
			$subject = $this->getSubject();
			$bodyMessage = wordwrap($this->getMessage(),65,"\n",FALSE);
			$headers = "From: ".$this->getSender(). "\n";
			
			return mail($to,$subject,$bodyMessage,$headers);
		}
	}
?>