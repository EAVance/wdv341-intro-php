<?php
	$yourName = "Erin Vance";
	$number1 = 9;
	$number2 = 58;
	$total = 0;
	
	$total = $number1 + $number2;
?>
<!DOCTYPE html>
<html>

	<!--
		Erin Vance
		WDV341 Intro PHP
		9/6/17
	-->

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>WDV341 Intro PHP - PHP Basics</title>
		<style>
			#container {
				margin:50px auto;
				width:500px;
				background-color: #f5ebe0;
				padding:4% 20%;
				color:#423444;
				letter-spacing:1.2px;
				box-shadow: 0px 0px 12px 1px rgba(0,0,0,0.75);
				text-align:center;
			}
			li{
				list-style-type: none;
				font-weight:bold;
			}
			hr {
				background-color:#FFFFFF;
				height: 1px; 
				border: 0;
				margin-top:2%;
			}
		
		</style>
	</head>

	<body>
	  <div id="container">
		<h1>WDV341 Intro PHP</h1>
		<hr>
		
		<?php echo "<h1>PHP Basics</h1>"; ?>
		

		<h2>My name is: <?php echo $yourName; ?> !</h2>
		
		<h3><?php echo "The sum of:  " . $number1 . "  +  " . $number2 . " = " . $total; ?></h3>
		
		<script>
			var languages = [<?php echo " 'PHP', 'HTML', 'JavaScript' " ;?>];
				for(var i=0; i<languages.length; i++){
					document.write("<li>" + languages[i] + "</li>");
				}
		</script>
	
		<p>&nbsp;</p>
		<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/d9f5429f2cc3578e3dab90732eb5ca08ab7a072b/phpBasics.php?at=master&fileviewer=file-view-default'">View PHP</button>
      </div>
	</body>
	
</html>