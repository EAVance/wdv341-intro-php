<?php
	$deleteEventId = $_GET['event_id'];	//Pull the event_id from the GET parameter
	
	include 'HomeworkPageFiles/connectPDO.php';		//connects to the database
	
	$sql = "DELETE FROM wdv341_event WHERE event_id = :eventId";
		
	$stmt = $conn->prepare($sql);	//prepare the statement
		
	$stmt->bindParam(':eventId',$deleteEventId, PDO::PARAM_INT);	//bind the parameter to the statement
		
	if ( $stmt->execute() ){
		$message =  "<h3><span class='check'>&#x2714;</span> Requested record has been successfully deleted.</h3><p><a href='selectEvents2.php'>View Records</a></p>";	
	}else{
		$message = "<h3><span class='X'>&#x2718;</span> We encountered a problem with your delete request, please try again.</h3><p><a href='selectEvents2.php'>Try Again </a></p>";	
	}
		
	$conn = null;	//close the database connection
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:800i|Montserrat" rel="stylesheet">
	<style>
		body{
			background: #0b3e6f;
			color:#000000;
			font-size:1.1em;
			font-family: 'Montserrat', sans-serif;
			letter-spacing:1.2px;}
		h3{
			color:#d9d9d9;
			text-align:center;
			margin-top:4em;
			font-family: 'Merriweather Sans', sans-serif;}
		div{
			text-align:center;}
		a{
			background-color:rgba(11,62,111,.6);
			margin:.8em;
			padding:.3em .5em;
			border-radius: 4px;
			font-size:1.1em;
			color:#000000;
			text-decoration:none;
			border-top:none;
			border-bottom:none;
			border-left:2px solid #505050;
			border-right:2px solid #505050;
			box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);}
		a:hover{
			color:#d9d9d9;
			background-color:rgba(17,17,17,.2);
			box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.4);}
		.check{
			font-size:1.7em;
			color:#329932;}
		.X{
			font-size:1.7em;
			color:#4c0000;}
	</style>
</head>
<body>

	<div>
		<?php echo $message; ?>
	</div>

	<div><a href='https://bitbucket.org/EAVance/wdv341/src/c5c90fb3f06af283a02e3cc2f61601b2bef3f172/deleteEvent.php?at=master&fileviewer=file-view-default'>View PHP</a></div>
	
</body>
</html>

