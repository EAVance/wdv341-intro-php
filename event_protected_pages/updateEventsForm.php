<?php
session_start();

if ($_SESSION['validUser'] == "yes") {
	
	$inName = $inDescription = $inPresenter = $inDate = $inTime = $formSubmitDate = "";    
	$nameError = $descriptionError = $presenterError = $dateError = $timeError = $message = $e = "";  
	$formTitle = "Register Event";
	$validForm = false;
	require 'HomeworkPageFiles/connectPDO.php';	//CONNECT to the database

	if(isset($_POST["submit"])){ //-------------IF FORM HAS HAS BEEN SUBMITTED, GATHER INPUT, START VALIDATIONS--------------   
		$inName = $_POST['event_name'];
		$inDescription = $_POST['event_description'];
		$inPresenter = $_POST['event_presenter']; 
		$inDate = $_POST['event_date'];
		$inTime = $_POST['event_time'];
		$inID = $_POST['event_id'];	      //hidden field used for update form
		$inPhone = $_POST['Phone'];
		
		if($inID != ""){                     //if there is an event_id change form header to UPDATE
			$formTitle = "Update Event";   
		}else{
			$formTitle = "Register Event";   //if there is NO event_id change form header to REGISTER
		}
		
		//validating name, if empty or just spaces - form is invalid & error message displays, else sanitize string
		function validateName(){           
			global $validForm, $nameError, $inName;		
			if(trim($inName) == ""){
				$validForm = false;
				$nameError = "Event Name is Required.";
			}else{
				$inName = filter_var($inName, FILTER_SANITIZE_STRING);
			}
		}
		
		//validating description, if empty or spaces - form is invalid & error message displays, else sanitize string
		function validateDescription(){
			global $validForm, $descriptionError, $inDescription;
			if(trim($inDescription) == ""){
				$validForm = false;
				$descriptionError = "Event Description is Required.";
			}else{
				$inDescription = filter_var($inDescription, FILTER_SANITIZE_STRING);
			}
		}
	
		//validating presenter, if empty or just spaces - form is invalid & error message displays, else sanitize string
		function validatePresenter(){           
			global $validForm, $presenterError, $inPresenter;		
			if(trim($inPresenter) == ""){
				$validForm = false;
				$presenterError = "Event Presenter is Required.";
			}else{
				$inPresenter = filter_var($inPresenter, FILTER_SANITIZE_STRING);
			}
		}
		
		//validate date, if blank - form is invalid & error message displays
		function validateDate(){
			global $validForm, $dateError, $inDate;
			if($inDate == ""){
				$validForm = false;
				$dateError = "Event Date is Required.";
			}
		}
		
		//validate time, if blank - form is invalid & error message displays
		function validateTime(){
			global $validForm, $timeError, $inTime;
			if($inTime == ""){
				$validForm = false;
				$timeError = "Event Time is Required.";
			}
		}
		
		//validate phone, if not blank - form is invalid
		function validatePhony(){
			global $validForm, $inPhone;
			if($inPhone){
				$validForm = false;
			}
		}
	
		$validForm = true;       
		//calling validation functions
		validateName(); 
		validateDescription();
		validatePresenter();
		validateDate();
		ValidateTime();
		validatePhony();
			
			
		if($validForm){ //-----------------IF VALID FORM CONTINUE--------------------
				
			if($inID != ""){ //------------IF EVENT UPDATE DO THIS----------------
				
				try {
					//Create the Update SQL command string
					$updateSQL = "UPDATE wdv341_event SET event_name= :eventName, event_description= :eventDescription, event_presenter= :eventPresenter, event_date= :eventDate, event_time= :eventTime WHERE event_id = :eventID";
					
					//PREPARE the SQL statement
					$stmt = $conn->prepare($updateSQL);
					
					//BIND the values to the input parameters of the prepared statement
					$stmt->bindParam(':eventName', $inName);
					$stmt->bindParam(':eventDescription', $inDescription);		
					$stmt->bindParam(':eventPresenter', $inPresenter);		
					$stmt->bindParam(':eventDate', $inDate);		
					$stmt->bindParam(':eventTime', $inTime);
					$stmt->bindParam(':eventID', $inID);
					
					//EXECUTE the prepared statement
					$stmt->execute();
					$message = "Event Update Successful";
					
					$conn = null;
				}catch(PDOException $e){
					$message = "There has been a problem. The system administrator has been contacted. Please try again later.  1";
				}
			}else{ //------------------IF NEW EVENT INSERT DO THIS------------------
				
				try {
					//Create the SQL command string
					$sql = "INSERT INTO wdv341_event (";    
					$sql .= "event_name, ";             
					$sql .= "event_description, ";
					$sql .= "event_presenter, ";
					$sql .= "event_date, ";
					$sql .= "event_time ";   
					$sql .= ") VALUES (:eventName, :eventDescription, :eventPresenter, :eventDate, :eventTime)";
					
					//PREPARE the SQL statement
					$stmt = $conn->prepare($sql);
					
					//BIND the values to the input parameters of the prepared statement
					$stmt->bindParam(':eventName', $inName);
					$stmt->bindParam(':eventDescription', $inDescription);		
					$stmt->bindParam(':eventPresenter', $inPresenter);		
					$stmt->bindParam(':eventDate', $inDate);		
					$stmt->bindParam(':eventTime', $inTime);	
					
					//EXECUTE the prepared statement
					$stmt->execute();	
					$message = "Event Successfully Registered";
					
					$conn = null;
				}catch(PDOException $e){
					$message = "There has been a problem. The system administrator has been contacted. Please try again later.   2";
				}
			}	
		}else{ //----------------------IF INVALID FORM DISPLAY ERROR MESSAGE & FORM--------------------------
			$message = "Something went wrong. Please Try Again.";
		}//ends check for valid form	
	}else{ //---------------------IF FORM HAS NOT BEEN SEEN AND NOT SUBMITTED DO THIS------------------------
		$inID = $_GET['event_id'];
		
		if($inID != ""){                    //if there is an event_id change form header to UPDATE
			$formTitle = "Update Event";
		}else{                              //if there is no event_id change form header to REGISTER
			$formTitle = "Register Event";
		}
		
		if(isset($inID)){ //-------------------IF EVENT ID IS PRESENT FROM $_GET, SHOW EVENT DATA FOR UPDATE----------------
			try {
				$stmt = $conn->prepare('SELECT event_name, event_description, event_presenter, event_date, event_time FROM wdv341_event WHERE event_id = :eventID');
				$stmt->bindParam(':eventID', $inID);
				$stmt->execute();
				$row = $stmt->fetch();
				if($row){
					$inName = $row["event_name"];
					$inDescription = $row["event_description"];
					$inPresenter = $row["event_presenter"]; 
					$inDate = $row["event_date"];
					$inTime = $row["event_time"];
				}else {
					$displayErrorMsg = "<h3><em>Zero results were found</em></h3>";			
				}	
				
				$conn = null;
			}catch(PDOException $e){
				$displayErrorMsg = "<h3><em>Sorry there has been a problem.</em></h3>";
			}
		}else{ 
		
		}
	}
}else{
	header('Location: login.php');
}
;?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:800i|Montserrat" rel="stylesheet">
	<!--CSS-->
	<link rel="stylesheet" href="HomeworkPageFiles/jquery-ui-dot-lov-theme/jquery-ui.css">
	<link rel="stylesheet" href="HomeworkPageFiles/jquery-timepicker-1.3.5/jquery.timepicker.min.css">
	<link rel="stylesheet" href="HomeworkPageFiles/updateEventsForm.css">
	<!--JS-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="HomeworkPageFiles/jquery-3.2.1.min.js"></script>
	<script src="HomeworkPageFiles/jquery-ui-dot-lov-theme/jquery-ui.js"></script>
	<script src="HomeworkPageFiles/jquery-timepicker-1.3.5/jquery.timepicker.min.js"></script>
	<script>
		$(document).ready(function(){
			$( function() {
				$( "#datepicker" ).datepicker({      
					showAnim: "slide",               //datepicker show animation turned to slide in and out
					changeMonth: true,               //allows user to pick month in drop down
					changeYear: true,                //allows user to pick year in drop down
					minDate: 0,                      //restricts the datepicker range by not allowing dates before today to be picked
					dateFormat: "yy-mm-dd",          //format date
				});
			} );
			$( function() {
				$( document ).tooltip();
			} );
			$('input.timepicker').timepicker({		//http://timepicker.co/	
				timeFormat: 'h:mm:ss p',			//time format 00:00:00 PM/AM
				scrollbar:true, 
			});     
		});
	</script>
</head>
<body>
	<!--Main section-->
	<div class="container">
	
		<p class="formButtons">
			<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/updateEventsForm.php'">Register Event</button>
			<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/selectEvents3.php'">Find Event</button>
			<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/login.php'">Login/Logout</button>
		</p>
	  
	<?php      
		if ($validForm) { //if valid form remove form and display message
			
	?>
	
			<div id="submitSuccess">
				<div class="successMsg">
					<h3><?php echo $message ?></h3> 
				</div>
				
				<?php
					if($e == ""){ //if valid form but there are database errors don't display input data
				?>
					<div id="custInput">
						<ul>
							<li><strong>Event Name:</strong> <em><?php echo $inName;?></em></li>
							<li><strong>Event Description:</strong> <em><?php echo $inDescription;?></em></li>
							<li><strong>Event Presenter:</strong> <em><?php echo $inPresenter;?></em></li>
							<li><strong>Event Date:</strong> <em><?php echo $inDate;?></em></li>
							<li><strong>Event Time:</strong> <em><?php echo $inTime;?></em></li>
						</ul>
					</div>
				<?php	 		
					}
				?>
				
			</div>
			<p class="formButtons">
				<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/2e43538851e55660bb181888548502ec43ccb8ea/updateEventsForm.php?at=master&fileviewer=file-view-default'">View PHP</button>
			</p>
	<?php	 		
		}else { //if not valid form display form again with previously entered event info
	?>
			
			<h2><?php echo $formTitle;?></h2>
	 
			<!--Form section-->		
			<form  id="eventsForm" name="eventsForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				
				<fieldset>
					<label>Event Name <br><span class="error"><?php echo $nameError; ?></span></label>
						<input type="text" name="event_name" id="eventName" size="25" value="<?php echo $inName;?>">
				</fieldset>
				<fieldset>
					<label>Event Description <br><span class="error"><?php echo $descriptionError; ?></span></label>
						<input type="text" name="event_description" id="description" size="25" value="<?php echo $inDescription;?>">
				</fieldset>
				<fieldset>
					<label>Event Presenter <br><span class="error"><?php echo $presenterError; ?></span></label>
						<input type="text" name="event_presenter" id="presenter" size="25" value="<?php echo $inPresenter;?>">						
				</fieldset>
				<fieldset>	
					<label>Event Date <br><span class="error"><?php echo $dateError; ?></span></label>
						<input type="text" id="datepicker" name="event_date" size="25" value="<?php echo $inDate;?>">	
				</fieldset>
				<fieldset>
					<label>Event Time <br><span class="error"><?php echo $timeError; ?></span></label>
						<input type="text" name="event_time" id="time" class="timepicker" size="25" value="<?php echo $inTime;?>">	
				</fieldset>
				<fieldset>
					<input type="hidden" name="event_id" id="id" value="<?php echo $inID; ?>" />
					<div id="phony">
						<label>Phone:</label>
						<input type="text" name="Phone">
					</div>											
				</fieldset>
				<div class="formButtons">
					<input type="submit" name="submit" value="SUBMIT" id="submit">
					<input type="reset" name="reset" value="RESET" id="reset">
				</div>
		
			</form><!--end form-->
	  
	<?php
		}      // end else 
	?>
	  
	</div><!--end main container -->

</body>
</html>