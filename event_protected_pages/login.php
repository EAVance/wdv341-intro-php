<?php
session_cache_limiter('none');			//This prevents a Chrome error when using the back button to return to this page.
session_start();
 
	if ($_SESSION['validUser'] == "yes"){	                    //if valid user or already signed on, skip the rest
		$message = "Welcome Back <em>". $inUsername."</em> !";	//Create greeting for VIEW area		
	}else{
		if (isset($_POST['submitLogin']) ){			         //if login portion was submitted do the following
		
			$inUsername = $_POST['event_user_name'];	    //pull the username from the form
			$inPassword = $_POST['event_user_password'];	//pull the password from the form
			
			include 'HomeworkPageFiles/connectPDO.php';				        //Connect to the database

			$sql = "SELECT event_user_name,event_user_password FROM event_user WHERE event_user_name = :username AND event_user_password = :password";				
			
			$query = $conn->prepare($sql);                 //prepare the query
			
			$query->bindParam(':username', $inUsername);   //bind parameters to prepared statement
			$query->bindParam(':password', $inPassword);
			
			$query->execute();
			
			$query->fetch();	
			
			if ($query->rowCount() == 1 ){		           //If this is a valid user there should be ONE row only
			
				$_SESSION['validUser'] = "yes";				//this is a valid user so set your SESSION variable
				$message = "Welcome Back <em>". $inUsername."</em> !";
				
			}else{
				
				$_SESSION['validUser'] = "no";				//error in login, not valid user	
				$message = "<em>Username or Password is incorrect. <br>Please try again.</em>";
			}			
			
			$conn = null;
			
		}else{
			//user needs to see form
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>WDV341 Intro PHP - Login and Control Page</title>
	<style>
		body{
			background: #0b3e6f;
			color:#000000;
			font-size:1.1em;
			font-family: 'Montserrat', sans-serif;
			letter-spacing:1.2px;}
		h2{
			color:#d9d9d9;
			text-align:center;
			margin-top:.8em;
			font-family: 'Merriweather Sans', sans-serif;}
		div{
			text-align:center;
			width: 375px;
			margin:0 auto;
			border-radius: 5px;
			padding-top:1em;
			padding-bottom:1em;
			border-left:none;
			border-right:none;
			border-top:2px solid #505050;
			border-bottom:2px solid #505050;
			background:rgba(17,17,17,.2);}
		p{
			font-weight:bold;
			color:#000000;}
		input[type="text"],input[type="password"] {
			color:#0D0D0D;
			border-radius:4px;
			border-top:none;
			border-bottom:none;
			border-right:none;
			border-left:2px solid #505050;
			background-color:rgba(99,99,99,.08);
			font-size:1.2em;
			padding:.4em .5em .4em 1em;
			width: 15em;
			margin-top:2px;}
		a,input[type=submit],input[type=reset]{
			background-color:rgba(11,62,111,.6);
			margin:.8em;
			padding:.3em .5em;
			border-radius: 4px;
			font-size:1.1em;
			color:#000000;
			text-decoration:none;
			border-top:none;
			border-bottom:none;
			border-left:2px solid #505050;
			border-right:2px solid #505050;
			box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);}
		a:hover, input[type=submit]:hover,input[type=reset]:hover{
			color:#d9d9d9;
			background-color:rgba(17,17,17,.2);
			box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.4);}
		
		.label{
			text-align:left;
			padding-left:.8em;
			margin-bottom:.4em;}
		
	</style>
</head>

<body>
	
	<h2><?php echo $message?></h2>

<?php
	if ($_SESSION['validUser'] == "yes"){	//This is a valid user.  Show them the Administrator Page	
?>
		<div>
			<h2>Administrator Options</h2>
			<p><a href="updateEventsForm.php" >Register Event</a></p>
			<p><a href="selectEvents3.php">View Events</a></p>
			<p><a href="logout.php">LOGOUT</a></p>	
        </div>					
<?php
	}else{	//The user needs to log in.  Display the Login Form
?>
		<div>
			<h2>Administrator Login</h2>
            <form method="post" name="loginForm" action="login.php" >
                <p class="label">Username:</p> <input name="event_user_name" type="text" />
                <p class="label">Password:</p> <input name="event_user_password" type="password" />
                <p><input name="submitLogin" value="LOGIN" type="submit" /> <input name="" type="reset" value="RESET"/>&nbsp;</p>
            </form>
		</div>
                
<?php 
	}  //end of checking for a valid user			
?>

</body>
</html>