<?php
session_start();

if ($_SESSION['validUser'] == "yes") {
	require 'HomeworkPageFiles/connectPDO.php';	
	$displayMsg = $displayErrorMsg = "";
	
	try {
		$result = $conn->prepare('SELECT event_name, event_description, event_presenter, event_date, event_time, event_id FROM wdv341_event');
		$result->execute();
		if($result->rowCount() > 0){
			$displayMsg .= "<table><tr class='tableHeader'><td>Name</td><td>Description</td><td>Presenter</td><td>Date</td><td>Time</td></tr>";
			while($row = $result->fetch()) {
				$displayMsg .= "<tr><td>".$row['event_name']."</td>";
				$displayMsg .= "<td>".$row["event_description"]."</td>";
				$displayMsg .= "<td>".$row["event_presenter"]."</td>";
				$displayMsg .= "<td>".$row["event_date"]."</td>";
				$displayMsg .= "<td>".$row["event_time"]."</td>"; 
				$displayMsg .= "<td><a href='updateEventsForm.php?event_id=".$row['event_id']."' class='tooltip'><i class='fa fa-pencil fa-lg' aria-hidden='true'></i> <span class='tooltiptext'>Edit</span></a></td>"; 
				$displayMsg .= "<td><a href='deleteEvent.php?event_id=".$row['event_id']."' class='tooltip'><i class='fa fa-trash-o fa-lg' aria-hidden='true'></i> <span class='tooltiptext'>Delete</span></a></td></tr>";
			}
			$displayMsg .= "</table>";
		}else {
			$displayErrorMsg = "<h3><em>Zero results were found</em></h3>";			
		}	
	}catch(PDOException $e){
		$displayErrorMsg = "<h3><em>Sorry there has been a problem.</em><br>" . $e->getMessage()."</h3>";
	}
	$conn = null;
}else{
	header('Location: login.php');
}
;?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:800i|Montserrat" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="HomeworkPageFiles/selectEventsStyles.css">
	<script src="HomeworkPageFiles/jquery-3.2.1.min.js"></script>
	<style>
		td:nth-child(6),td:last-child {
			border-top:none;
			border-bottom:none;
			border-left:none;
			background-color:#0b3e6f;
			padding:0;}
	</style>
</head>
<body>
	
	<p class="formButtons">
		<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/updateEventsForm.php'">Register Event</button>
		<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/selectEvents3.php'">Find Event</button>
		<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/login.php'">Login/Logout</button>
	</p>
	
<?php      
	if ($result->rowCount() > 0) {	   //if query results have more than 0 records in database the events will show	
?>

	<h3>Registered Events</h3>
	<div id="content">
		<?php echo $displayMsg; ?>
	</div>
	
<?php	 		
	}else {   //if the query results are 0 records in database or error, message will show
?>

	<div>
		<?php echo $displayErrorMsg; ?>
	</div>
	
<?php
	}      // end else 
?>
	
	<div class="formButtons">
		<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/2e43538851e55660bb181888548502ec43ccb8ea/selectEvents3.php?at=master&fileviewer=file-view-default'">View PHP</button>
	</div>
	
</body>
</html>