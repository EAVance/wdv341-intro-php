<?php
	require 'HomeworkPageFiles/connectPDO.php';	
	$displayMsg = $displayErrorMsg = "";
	
	try {
		$sql = $conn->prepare('SELECT event_name, event_description, event_presenter, event_date, event_time FROM wdv341_event WHERE event_id = 1');
		$sql->execute();
		$row = $sql->fetch();
		if($row){
			$displayMsg .= "<table><tr class='tableHeader'><td>Name</td><td>Description</td><td>Presenter</td><td>Date</td><td>Time</td></tr>";
			$displayMsg .= "<tr><td>".$row["event_name"]."</td>";
			$displayMsg .= "<td>".$row["event_description"]."</td>";
			$displayMsg .= "<td>".$row["event_presenter"]."</td>";
			$displayMsg .= "<td>".$row["event_date"]."</td>";
			$displayMsg .= "<td>".$row["event_time"]."</td></tr></table>";
		}else {
			$displayErrorMsg = "<h3><em>Zero results were found</em></h3>";			
		}	
	}catch(PDOException $e){
		$displayErrorMsg = "<h3><em>Sorry there has been a problem.</em><br>" . $e->getMessage()."</h3>";
	}
	$conn = null;
;?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:800i|Montserrat" rel="stylesheet">
	<script src="HomeworkPageFiles/jquery-3.2.1.min.js"></script>
	<style>
		body{
			background: #0b3e6f;
			color:#000000;
			font-size:1.1em;
			font-family: 'Montserrat', sans-serif;
			letter-spacing:1.2px;}
		#content{
			width:80%;
			margin:0 auto;}
		h3{
			color:#d9d9d9;
			text-align:center;
			margin-top:4em;
			font-family: 'Merriweather Sans', sans-serif;}
		input[type="text"] {
			color:#d9d9d9;
			border-radius:4px;
			border-top:none;
			border-bottom:none;
			border-right:none;
			border-left:2px solid #505050;
			background-color:rgba(99,99,99,.08);
			font-size:1.2em;
			padding:.4em .5em .4em 1em;
			width: 16.5em;
			margin-top:2px;}
		.tableHeader{
			text-align:center;
			font-size:1.2em;
			font-weight:bold;}
		table{
			width:90%;
			margin:2em auto;
			font-size:.9em;
			border-collapse: collapse;}
		tr + tr { 
			border-top: 1px solid rgba(152,152,152,.3); }
		td{
			padding:4px;}
		td + td {
			border-left:1px solid rgba(152,152,152,.3);
			padding:.6em;}
		.formButtons {
			text-align:center;}
		button{
			margin:.8em;
			padding:.3em .5em;
			border-radius: 4px;
			font-size:1.1em;
			color:#000000;
			text-decoration:none;
			border-top:none;
			border-bottom:none;
			border-left:2px solid #505050;
			border-right:2px solid #505050;
			box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);
			background-color:rgba(17,17,17,.2);}
		button:hover{
			color:#d9d9d9;
			background-color:rgba(11,62,111,.6);
			box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.4);}
		@media only screen and (max-width: 1200px){
			#content{
				width:100%;}
		}
	</style>
</head>
<body>

<?php      
	if($row) {	   //if query results have more than 0 records in database the event will then show	
?>

	<h3>Selected Registered Event</h3>
	<div id="content">
		<?php echo $displayMsg; ?>
	</div>
	
<?php	 		
	}else {   //if the query results are 0 records in database or error, message will show
?>

	<div>
		<?php echo $displayErrorMsg; ?>
	</div>
	
<?php
	}      // end else 
?>
	<div class="formButtons">
		<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/c4dc20cba40ca41762fb6c506aa9e287ed1a9a75/selectOneEvent.php?at=master&fileviewer=file-view-default'">View PHP</button>
	</div>
	
</body>
</html>