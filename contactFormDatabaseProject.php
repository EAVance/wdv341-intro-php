<?php
	include 'Email1.php'; 
	$inName = $inEmail = $inContactReason = $inComments = $formSubmitDate = $formSubmitTime = $inMailList = $inProductInfo = $inPhone = "";    
	$nameError = $emailError = $contactReasonError = $commentsError = $message = "";   
	$validForm = false;

	//if form has been submitted gather user input and start validations
	if(isset($_POST["submit"])){         
		$inName = $_POST['contact_name'];
		$inEmail = $_POST['contact_email'];
		$inContactReason = $_POST['contact_reason']; 
		$inComments = $_POST['contact_comments'];
		$inMailList = (isset($_POST['contact_newsletter'])) ? 'Yes' : 'No';         //if checkbox is checked, yes is sent to database,if not checked, no is sent to database
		$inProductInfo = (isset($_POST['contact_more_products'])) ? 'Yes' : 'No';   //if checkbox is checked, yes is sent to database,if not checked, no is sent to database
		$inPhone = $_POST['Phone'];
		
		//validating name, if empty or just spaces - form is invalid & error message displays, else sanitize string
		function validateName(){           
			global $validForm, $nameError, $inName;		
			if(trim($inName) == ""){
				$validForm = false;
				$nameError = "Name is Required.";
			}else{
				return $inName = filter_var($inName, FILTER_SANITIZE_STRING);
			}
		}
		
		//validating email, if empty or spaces OR not a valid email - form is invalid & error message displays
		function validateEmail(){
			global $validForm, $emailError, $inEmail;
			if(trim($inEmail) == ""){
				$validForm = false;
				$emailError = "Email is Required.";
			}else if(!preg_match("/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/",trim($inEmail))){        
				$validForm = false;
				$emailError = "Invalid Email (eg. yourName@yahoo.com)";
			}
		}
	
		//validating contact reason, if unselected - form is invalid & error message displays
		function validateContactReason(){               
			global $validForm, $contactReasonError, $inContactReason;
			if($inContactReason == "default"){
				$validForm = false;
				$contactReasonError = "Please Select a Contact Reason<br>";
			}
		}
		
		//validating comments, not required unless user selects 'Other' for contact reason, else sanitize string
		function validateComments(){
			global $validForm, $commentsError, $inContactReason, $inComments;
			if($inContactReason == "Other" && trim($inComments) == ""){
				$validForm = false;
				$commentsError = "By selecting 'Other' for contact reason, you must enter a comment.";
			}else{
				return $inComments = filter_var($inComments, FILTER_SANITIZE_STRING);
			}
		}
		
		function validatePhony(){
			global $validForm, $inPhone;
			if($inPhone){
				$validForm = false;
			}
		}
	
		//date and time of form submission
		function formSubmitDate(){
			global $formSubmitDate,$formSubmitTime;
			date_default_timezone_set('America/Chicago');
			$formSubmitDate = date('m/d/y');
			$formSubmitTime = date('h:i A');
		}
		
		$validForm = true;       
		//calling validation functions
		validateName(); 
		validateEmail();
		validateContactReason();
		validatePhony();
		validateComments(); 
		formSubmitDate();		
		
		//Email set up for 'user/customer' confirmation email 
		$custConfirmationEmail = new Email();               //new Email Object
		$custConfirmationEmail->setRecipient($inEmail);    
		$custConfirmationEmail->setSender("contact@erinavance.info");         
		$custConfirmationEmail->setSubject("NexGen Astronomer Confirmation");
		$custConfirmationEmail->setMessage("
			<html>
			<head>
				<link href='https://fonts.googleapis.com/css?family=Expletus+Sans:700|Happy+Monkey' rel='stylesheet'>
				<style>
					#header {
						margin:.09em;
						text-align:left;
						text-shadow: 1px 2px 3px #323232; 
						color:#485454;
						font-family: 'Expletus Sans', cursive;
						letter-spacing:0.04em;}
					#main {
						background-color:#E8F0EE;
						color:#1A292E;
						margin:.1em 0 .1em 0;
						padding:.5em 2em;
						box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);
						letter-spacing:0.04em;}
					ul {
						list-style-type:none;
						padding-left:0px;}
				</style>
			</head>
			<body>
			  <div id='header'>
				<h1>NexGen Astronomer</h1>
			  </div>
			  <div id='main'>
				<h3>Dear <em style='color:#7c6c5d;'>".$inName."</em>,</h3>
				<h4>Thank you for your recent comments or questions.<br> We will review your input and be in contact with you shortly.</h4>
				<h4>You have successfully submitted the following information:</h4>
				<ul>
					<li><strong>Name:</strong> <em>".$inName."</em></li>
					<li><strong>Email:</strong> <em>".$inEmail."</em></li>
					<li><strong>Contact Reason:</strong> <em>".$inContactReason."</em></li>
					<li><strong>Comments:</strong> <em>".$inComments."</em></li>
					<li><strong>Mailing List:</strong> <em>".$inMailList."</em></li>
					<li><strong>More Information:</strong> <em>".$inProductInfo."</em></li>
					<li><strong>Date:</strong> <em>".$formSubmitDate."</em></li>
					<li><strong>Time:</strong> <em>".$formSubmitTime."</em></li>
				</ul>
				<h4><em style='color:#7c6c5d;'>NexGen Astronomer <br>Customer Service</em></h4>
			  </div>
			</body>
			</html>");
		
		//Email set up with customer information sent to 'website/business owner' 
		$custServiceEmail = new Email();                             //new Email Object
		$custServiceEmail->setRecipient("ealippert@dmacc.edu");    
		$custServiceEmail->setSender("contact@erinavance.info");         
		$custServiceEmail->setSubject("NexGen Astronomer Customer Contact Form Submission");
		$custServiceEmail->setMessage("
			<html>
			<head>
				<style>
					#main{
						letter-spacing:0.04em;
						background-color:#E8F0EE;
						color:#1A292E;
						margin:.1em;
						padding:.5em 1em;}
					ul {
						list-style-type:none;
						padding-left:0px;
						width:400px;}
					li {
						border-bottom: 1px solid #c2c6c6;
						padding:4px;}
				</style>
			</head>
			<body>
			  <div id='main'>
				<h3 style='color:#344141;'>Customer Information:</h3>
				<ul>
					<li><strong>Name:</strong> <em>".$inName."</em></li>
					<li><strong>Email:</strong> <em>".$inEmail."</em></li>
					<li><strong>Contact Reason:</strong> <em>".$inContactReason."</em></li>
					<li><strong>Comments:</strong> <em>".$inComments."</em></li>
					<li><strong>Mailing List:</strong> <em>".$inMailList."</em></li>
					<li><strong>More Information:</strong> <em>".$inProductInfo."</em></li>
					<li><strong>Date:</strong> <em>".$formSubmitDate."</em></li>
					<li><strong>Time:</strong> <em>".$formSubmitTime."</em></li>
				</ul>
			  </div>
			</body>
			</html>");
		
		//if valid form submit customer info to database
		if($validForm){
			try {
				require 'HomeworkPageFiles/connectPDO.php';	//CONNECT to the database		
				
				//Create the SQL command string
				$sql = "INSERT INTO wdv_341_customer_contacts(";    
				$sql .= "contact_name, ";             
				$sql .= "contact_email, ";
				$sql .= "contact_reason, ";
				$sql .= "contact_comments, ";
				$sql .= "contact_newsletter, ";
				$sql .= "contact_more_products, "; 
				$sql .= "contact_date, ";
				$sql .= "contact_time ";
				$sql .= ") VALUES (:contactName, :contactEmail, :contactReason, :contactComments, :contactNewsletter, :contactProductInfo, :contactDate, :contactTime)";
				
				//PREPARE the SQL statement
				$stmt = $conn->prepare($sql);
				
				//BIND the values to the input parameters of the prepared statement
				$stmt->bindParam(':contactName', $inName);
				$stmt->bindParam(':contactEmail', $inEmail);		
				$stmt->bindParam(':contactReason', $inContactReason);		
				$stmt->bindParam(':contactComments', $inComments);		
				$stmt->bindParam(':contactNewsletter', $inMailList);
				$stmt->bindParam(':contactProductInfo', $inProductInfo);				
				$stmt->bindParam(':contactDate', $formSubmitDate);	
				$stmt->bindParam(':contactTime', $formSubmitTime);	
				
				//EXECUTE the prepared statement
				$stmt->execute();	
				
			}catch(PDOException $e){
				$message = "There has been a problem. The system administrator has been contacted. Please try again later.";   
			}
		}else{
			$message = "Invalid entry. Please try again.";    //if invalid form, displays error message if any user input is invalid
		}//ends check for valid form		
	}
;?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!--
		Erin Vance
		WDV341 Contact Form With Database Project
		11/6/2017
	-->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Contact Form with Database Project</title>
	<!-- Bootstrap CSS -->
	<link href="HomeworkPageFiles/bootstrap.min.css" rel="stylesheet">
	<link href="HomeworkPageFiles/sticky-footer.css" rel="stylesheet">
	<!--Custom Style Sheets--->
	<link href="HomeworkPageFiles/contactFormProjectStyles.css" rel="stylesheet">
	<!--Icons Font Awesome-->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!--Text Styles Google Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:600i|Questrial|Ubuntu:700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Expletus+Sans:700|Happy+Monkey" rel="stylesheet">
	<style>
		.error {
			color:#cc8300;}
		#phony {
			display: none;}
		#custInput {
			font-family:'Questrial', sans-serif;
			font-size:1.3em;
			letter-spacing:0.05em;}
		#custInput ul{
			list-style-type:none;
			padding-top:2em;
			padding-left:0px;}
		#custInput li {
			padding-top:4px;}
		#submitSuccess {
			margin:3em 1em;}
		.successMsg{
			padding-top:1em;}
		.successPgButtons{
			text-align:center;}
	</style>
	<!--JS-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="HomeworkPageFiles/bootstrap.min.js"></script>
</head>
<body>

	<!-- Header section -->
	<div class="jumbotron">
		<section id="top"></section>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					  <span class="sr-only">Toggle navigation</span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					</button>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
					  <li><a href="#">Home</a></li>
					  <li><a href="#">About</a></li>
					  <li><a href="#">Magazine</a></li>
					  <li><a href="#">Subscribe</a></li>
					  <li><a href="#">Products</a></li>
					  <li class="active"><a href="#">Contact</a></li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>
	  
		<a href="#"><h1>NexGen Astronomer</h1></a>
		<h2>Inspiring the next generation of explorers</h2>
	</div><!--end jumbotron header -->
	
	<!--Main section-->
	<div class="container">
		<div class="row">
	  
	<?php      
		if ($validForm) {	                     //if valid form remove form and display thank you message & submitted info	
			$custConfirmationEmail->sendMail();  //if valid send a confirmation email to customer - using Email class
			$custServiceEmail->sendMail();       //if valid send customer service customers submitted information - using Email class
	?>
			<div id="submitSuccess">
				<div class="col-md-8 successMsg">
					<h3>Thank you <span style='color:#5c6666;'><?php echo $inName;?></span>,</h3> 
					<h3>You should be receiving an email confirmation to <span style='color:#5c6666;'><?php echo $inEmail;?></span>. <br> We will be in contact with you shortly. </h3>
					<h3>You have successfully submitted the following information:</h3>
				</div>
				<div id="custInput" class="col-sm-12 col-md-4">
					<ul>
						<li><strong>Name:</strong> <em><?php echo $inName;?></em></li>
						<li><strong>Email:</strong> <em><?php echo $inEmail;?></em></li>
						<li><strong>Contact Reason:</strong> <em><?php echo $inContactReason;?></em></li>
						<li><strong>Comments:</strong> <em><?php echo $inComments;?></em></li>
						<li><strong>Mailing List:</strong> <em><?php echo $inMailList;?></em></li>
						<li><strong>More Information:</strong> <em><?php echo $inProductInfo;?></em></li>
						<li><strong>Date:</strong> <em><?php echo $formSubmitDate;?></em></li>
						<li><strong>Time:</strong> <em><?php echo $formSubmitTime;?></em></li>
					</ul>
				</div>
			</div>
	
			<p class="successPgButtons col-sm-12">
				<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/contactFormDatabaseProject.php'">Back to Form</button>
				<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/0555a5385164882cfa5f44536df2a5a98b45be9f/contactFormDatabaseProject.php?at=master&fileviewer=file-view-default'">View PHP</button>
			</p>
		
	<?php	 		
		}else {      //if not valid form, display form again with previously entered cust info
	?>
	  
			<h3>Comments / Questions</h3>
			<h3 class="error" style="text-align:center;"><?php echo $message ;?></h3>
			<!--Form section-->		
			<form class="form-horizontal" id="contactForm" name="contactForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				
				<div class="form-group">
					<label class="col-sm-12 col-md-2 col-form-label">Name</label>
					<div class=" col-md-10">
						<span class="error"><?php echo $nameError; ?></span>
						<input type="text" name="contact_name" id="name" class="form-control" value="<?php echo $inName;?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-12 col-md-2 col-form-label">Email</label>
					<div class=" col-md-10">
						<span class="error"><?php echo $emailError; ?></span>
						<input type="text" name="contact_email" id="email" class="form-control" placeholder="youremail@google.com"  value="<?php echo $inEmail;?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-12 col-md-2 col-form-label">Contact Reason</label>
					<div class="col-md-10">
						<span class="error"><?php echo $contactReasonError; ?></span>
						<select name="contact_reason" id="contactReason" class="form-control">
							<option value="default" <?php if($inContactReason == "default") { echo "selected='selected'"; } ?>>( Please Select a Reason )</option>
							<option value="Product" <?php if($inContactReason == "Product") { echo "selected='selected'"; } ?>>Product Problem</option>
							<option value="Return" <?php if($inContactReason == "Return") { echo "selected='selected'"; } ?>>Return a Product</option>
							<option value="Billing" <?php if($inContactReason == "Billing") { echo "selected='selected'"; } ?>>Billing Question</option>
							<option value="Subscription" <?php if($inContactReason == "Subscription") { echo "selected='selected'"; } ?>>Subscription Question</option>
							<option value="Other" <?php if($inContactReason == "Other") { echo "selected='selected'"; } ?>>Other - If 'Other' please state reason in 'Comments' section.</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-12 col-md-2 col-form-label">Comments</label>
					<div class=" col-md-10">
						<span class="error"><?php echo $commentsError; ?></span>
						<textarea name="contact_comments" id="commentTextarea" rows="3" class="form-control" ><?php echo $inComments;?></textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 col-form-label"></label>
					<div class="col-sm-10 col-md-10">
						<div class="form-check">
							<label class="form-check-label active">
								<input type="checkbox" name="contact_newsletter" id="mailListCheck" class="form-check-input" value="Yes"  <?php if($inMailList == 'Yes') {echo 'checked';} ?>> Please put me on your mailing list.
							</label>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 col-md-2 col-form-label"></label>
					<div class="col-sm-10 col-md-10">
						<div class="form-check">
							<label class="form-check-label">
								<input type="checkbox" name="contact_more_products" id="moreInfoCheck" class="form-check-input" value="Yes"  <?php if($inProductInfo == 'Yes') {echo 'checked';} ?>> Send me more information about your products.
							</label>
						</div>
					</div>
				</div>
				
				<div id="phony">
					<label>Phone:</label>
					<input type="text" name="Phone">
				</div>											
				
				<div class="form-group">
					<div class="col-12 text-center">
						<button type="reset" name="reset" value="Reset" id="reset">Reset</button>
						<button type="submit" name="submit" value="Submit" id="submit">Submit</button>
					</div>
				</div>
		
			</form><!--end form-->
	  
	<?php
		}      // end else 
	?>
	  
		</div><!--end row-->
	
	   <p id="btn2"><a id="topButton" href="#top">^</a></p>
	</div><!--end main container -->
	
	<!--Footer section-->
	<footer class="footer">
		<div class="container">
			<a href="#" class="fa fa-facebook-square fa-2x"></a>
			<a href="#" class="fa fa-twitter-square fa-2x"></a>
			<a href="#" class="fa fa-instagram fa-2x"></a>
			<a href="#" class="fa fa-pinterest-square fa-2x"></a>
			<p class="text-muted">© <script>document.write(new Date().getFullYear());</script> <span>NexGen Astronomer  </span>  |  All Rights Reserved</p>
		</div>
	</footer>

</body>
</html>