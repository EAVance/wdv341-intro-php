<?php
	$inName = $inSoc = "";    
	$nameError = $socError = $responseTypeError = "";   
	$validForm = false;

	//if form has been submitted gather user input and start validations
	if(isset($_POST["submit"])){         
		$inName = $_POST['Name'];
		$inSoc = $_POST['Soc'];
		
		//validating name, if empty or just spaces - form is invalid & error message displays
		function validateName($name){           
			global $validForm, $nameError;		
			$name = trim($name);
			if($name == ""){
				$validForm = false;
				$nameError = "Name is Required.";
			}
		}
		
		//validating SSN, if empty or has anything other than 9 numbers & starts w/ 0 - form is invalid & error message displays
		function validateSoc($soc){                   
			global $validForm,$socError;
			if($soc == ""){
				$validForm = false;
				$socError = "Social Security Number is Required.";
			}else if(!preg_match("/^[1-9][0-9]{9}$/",$soc)){
				$validForm = false;
				$socError = "No Special Characters or Spaces. <strong><br>9</strong> Numbers ONLY.";
			}
		}
		
		//validating radio response type, if unselected - form is invalid & error message displays
		function validateResponseType(){               
			global $validForm,$responseTypeError;
			if(!isset($_POST['ResponseType'])){
				$validForm = false;
				$responseTypeError = "Please Select a Response<br>";
			}
		}
	
		$validForm = true;       
		//calling validation functions
		validateName($inName);       
		validateSoc($inSoc);
		validateResponseType();
	}
;?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>WDV341 Intro PHP - Form Validation & Self Posting</title>
	<link href="https://fonts.googleapis.com/css?family=Prosto+One|Questrial|Rammetto+One" rel="stylesheet">
	<link href="HomeworkPageFiles/formValidationStyles.css" rel="stylesheet">
	<style></style>
	<script src="HomeworkPageFiles/jquery-3.2.1.min.js"></script>
	<script>
		$("#reset").click(function(){
			$("#Name,#Soc").val("");
		});
	</script>
</head>

<body>
		<h2>Form Validation Assignment</h2>
	
	<?php      
		if ($validForm) {	//if valid form remove form and display thank you message & buttons	
	?>
	
		<h3>Thank you, you have successfully registered!</h3>
		
		<p class="formButtons">
			<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/formValidationAssign.php'">Back to Form</button>
			<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/b76f85afe473aab70749465f681ff9b2e830b851/formValidationAssignment.php?at=master&fileviewer=file-view-default'">View PHP</button>
		</p>
		
	<?php	 		
		}else {      //if not valid form display form again with previously entered cust info
	?>
	
		
		<form id="form1" name="form1" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
			<h3>Customer Registration Form</h3>
		
			<div id="formArea">
				<p>
					<label class="font">Name </label><br>
					<span width="210" class="error"><?php echo $nameError; ?></span>
					<input type="text" name="Name" id="Name" size="30" value="<?php echo $inName;?>"/>
				</p>	
				<p>
					<label class="font">Social Security </label><br>
					<span class="error"><?php echo $socError; ?></span>
					<input type="text" name="Soc" id="Soc" size="30" value="<?php echo $inSoc;?>"/>
				</p>	
				<p>
					<label class="font">Response Type </label><br>
					<span class="error"><?php echo $responseTypeError; ?></span>
						<label>
					  <input type="radio" name="ResponseType" id="ResponseType1" value="Phone" <?php if (isset($_POST['ResponseType']) && $_POST['ResponseType']=='Phone' ){echo ' checked="checked"';}?>>
					  Phone</label>
					<br>
					<label>
					  <input type="radio" name="ResponseType" id="ResponseType2" value="Email" <?php if (isset($_POST['ResponseType']) && $_POST['ResponseType']=='Email' ){echo ' checked="checked"';}?>>
					  Email</label>
					<br>
					<label>
					  <input type="radio" name="ResponseType" id="ResponseType3" value="Mail" <?php if (isset($_POST['ResponseType']) && $_POST['ResponseType']=='Mail' ){echo ' checked="checked"';}?>>
					  US Mail</label>
					<br>
				</p>
			</div>	  
			
			<p class="formButtons">
				<input type="submit" name="submit" id="submit" value="Register" />
				<input type="reset" name="reset" id="reset" value="Clear Form" />
		    </p>
		</form>
		
		
	<?php
		}      // end else 
	?>
</body>
</html>