<?php
	include 'Email.php';            //copies/imports Email Class from file Email.php
	
	$contactEmail = new Email();    //instantiates/creates new Email object called $contactEmail (copy of Email Class)
	
	$contactEmail->setRecipient("yourName@yahoo.com");      //Real Recipient Email goes here
	$contactEmail->setSender("myName@yahoo.com");           //Real Sender Email goes here
	$contactEmail->setSubject("OOP Email Class Example");
	$contactEmail->setMessage("Lorem ipsum dolor sit amet, intellegam conclusionemque eum id, est habemus indoctum ei. Pri tollit signiferumque ei, 
	adhuc dicat no duo, exerci ancillae delicatissimi ad sit. Et vix ullum dolor facilis. Sit nihil molestiae ei, liber dolor ut his, est oratio 
	phaedrum vituperata at. Insolens principes nam et, alii aeterno impedit mea eu. <br>Melius aliquando vix ut, per ea prima ubique. At habeo tollit 
	interpretaris pro, pri enim etiam scribentur ex, vim ex verear repudiare. Mea ea scriptorem appellantur voluptatibus. Mea te saepe deseruisse, 
	mandamus imperdiet ut per.!");
	
	$emailStatus = $contactEmail->sendMail();
	if($emailStatus){
		$emailStatus = ("Email was successfully sent!");
	}else{
		$emailStatus = ("Something went wrong email was not delivered!");   //Email sending will fail until you change Recipient & Sender Emails to real ones
	}
?>
<!doctype html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="https://fonts.googleapis.com/css?family=Open+Sans|Rambla:700i" rel="stylesheet">
		<style>
			html{
				min-height: 100%
			}
			p{
				font-family: 'Open Sans', sans-serif;
				letter-spacing: 1.2px;
			}
			h2{
				text-align:center;
				font-family: 'Rambla', sans-serif;
				letter-spacing: 1.2px;
			}
			body{
				margin:5em 8em;
				background: #636d53;
				background: -moz-linear-gradient(-45deg, #636d53 2%, #006666 50%, #006666 50%, #636d53 100%);
				background: -webkit-linear-gradient(-45deg, #636d53 2%,#006666 50%,#006666 50%,#636d53 100%);
				background: linear-gradient(135deg, #636d53 2%,#006666 50%,#006666 50%,#636d53 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#636d53', endColorstr='#636d53',GradientType=1 );
			}
			#phpButtons{
				text-align:center;
			}
			button{
				border-radius: 4px;
				border:1px solid #000000;
				background:rgba(0,0,0,0.5);
				box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);
				padding:.2em .4em;
				margin:1em .5em;
				color:white;
				font-family: 'Open Sans', sans-serif;
				letter-spacing: 1.2px;
			}
			button:hover{
				background:rgba(0,0,0,0.6);
				box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.4);
				color:white;
			}
		</style>
	</head>
	<body>
		<h2>OOP PHP Email Class - Example</h2>
		
		<p><strong>Recipient Email Address:</strong> <br><?php echo $contactEmail->getRecipient();?></p>
		<p><strong>Sender Email Address:</strong> <br><?php echo $contactEmail->getSender();?></p>
		<p><strong>Subject:</strong> <br><?php echo $contactEmail->getSubject();?></p>
		<p><strong>Message:</strong> <br><?php echo $contactEmail->getMessage();?></p>
		<p><strong>Email Status:</strong> <br><?php echo $emailStatus ?></p>
		
		<div id="phpButtons">
			<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/12cd708eb2d4bd9e4ccf08d3fabe020b36fea325/processEmail.php?at=master&fileviewer=file-view-default'">View processEmail PHP</button>
			<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/779303df1c03b24ce870910f5af4daa0c52ab405/Email.php?at=master&fileviewer=file-view-default'">View Email class PHP</button>
		</div>
	</body>
</html>