<?php
//Model-Controller Area.  The PHP processing code goes in this area. (getting data set up)

	//Method 1.  This uses a loop to read each set of name-value pairs stored in the $_POST array
	$tableBody = "";		//use a variable to store the body of the table being built by the script
	
	foreach($_POST as $key => $value)		//foreach is an "iterator" more powerful loop(foreach row in array run this)/This will loop through each name-value in the $_POST array
	{                                       //$_POST is an associative array
		$tableBody .= "<tr>";				//formats beginning of the row, . is a concatnater(add together) for php
		$tableBody .= "<td>$key</td>";		//dsiplay the name of the name-value pair from the form
		$tableBody .= "<td>$value</td>";	//dispaly the value of the name-value pair from the form
		$tableBody .= "</tr>";				//End this row
	} 
	
	
	//Method 2.  This method pulls the individual name-value pairs from the $_POST using the name
	//as the key in an associative array.  
	
	$inFirstName = $_POST["First_Name"];		//Get the value entered in the first name field,  global variable bc not in function
	$inLastName = $_POST["Last_Name"];		//Get the value entered in the last name field
	$inSchool = $_POST["School"];			//Get the value entered in the school field
	$inGender = $_POST["Gender"];
	$inYear = $_POST["Year"];
	$inFPTime = $_POST["FT/PT"];
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>WDV 341 Intro PHP - Code Example</title>
	<style>
		#container{
			width:375px;
			margin:0 auto;
			text-align:center;
			color:#333333;
			font-size:1.1em;
			border: 1px solid #CAB6AC;
			padding:2em;
		}
		table, td, th {    
			border: 1px solid #CAB6AC;
			text-align: left;
		}
		table {
			border-collapse: collapse;
			width: 100%;
		}
		th, td {
			padding: 15px;
		}
		h3 {
			font-style:italic;
		}
		h2 {
			color:#CAB6AC;
		}
	</style>
</head>

<body>
  <div id="container">
	<h2>Form Handler Result Page</h2>

	<h3>Method 1</h3>
		<p>
			<table border='a'>
				<tr>
					<th>Field Name</th>
					<th>Value of Field</th>
				</tr>
				<?php echo $tableBody;  ?>
			</table>
		</p>
	
	<h3>Method 2</h3>

		<p>First Name: <?php echo $inFirstName; ?></p>
		<p>Last Name: <?php echo $inLastName; ?></p>
		<p>School: <?php echo $inSchool; ?></p>
		<p>Genter: <?php echo $inGender; ?></p>
		<p>Year: <?php echo $inYear; ?></p>
		<p>FT/PT: <?php echo $inFPTime; ?></p>
  </div>
</body>
</html>
