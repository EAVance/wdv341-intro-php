<?php
	$inCustName = $_POST["Name"];		
	$inCustEmail = $_POST["Email"];		
	$inCustComments = $_POST["Comments"];

	//Email set up for customer confirmation email
	$toCustEmail = $inCustEmail;				
	$subject = "Contact Form With Email ASSIGNMENT";
	$fromEmail = "contact@erinavance.info";		
	$custEmailBody = "
    <html>
    <head>
        <style>
			body{
				background-color:#CAD0D3;
			}
			div{
				background:rgba(255,255,255, 0.7);
				color:#6C7485;
				margin:.1em;
				padding:.5em 2em;
				box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);
			}
		</style>
    </head>
    <body>
	  <div>
        <h3>Dear <em style='color:#8ec8c9;'>".$inCustName."</em>,</h3>
		<h4>Thank you for your comments or questions.<br> We will review your input and be in contact with you shortly.</h4>
		<h4><em style='color:#8ec8c9;'>Customer Service</em></h4>
	  </div>
    </body>
    </html>";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "From: $fromEmail" . "\r\n";			
	
	//If email was accepted for delivery a 'successfully sent' confirmation message will display, else a 'message did not deliver' will display
 	if (mail($toCustEmail,$subject,$custEmailBody,$headers)) 	
	{
   		echo("<div style='margin:0 auto;text-align:center;'><h2>Thank you <em style='color:#8ec8c9;'>".$inCustName."</em>,<br><br>Your message was successfully sent!<br>We will be sending a confirmation email to <em style='color:#8ec8c9;'>".$inCustEmail."</em> shortly!</h2></div>");
  	} 
	else 
	{
   		echo("<div style='margin:0 auto;text-align:center;'><h2>Sorry something went wrong, your message did not deliver ...</h2></div>");
  	}
	
	//Email set up with name value pairs(customer information) sent to website owner
	$custInput = "<h3 style='color:#CAB6AC;'>Customer Information:</h3><br>";
	foreach($_POST as $key => $value)											
	{
		$custInput.= "<div style='border: 1px solid #CAB6AC;border-radius: 4px;padding:5px;margin:5px;color:#8E8A87;'>".$key."  :  ".$value."</div>\n";
	}
	mail("ealippert@dmacc.edu",$subject,$custInput,$headers);
	
;?>
<!DOCTYPE html>
<html>
  <head>
	<style>
		html{
			background-color:#CAD0D3;	
		}
		body {
			background:rgba(255,255,255, 0.8);
			color:#6C7485;
			margin:1em;
			box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.4);
			padding:3em 4em 2em 4em;
		}
		#formButtons {
			text-align:center;
			margin-top:2em;
		}
		button{
			background-color:#CAD0D3;
			color:#000000;
			border-radius: 4px;
			border:1px solid #93c3cd;
			box-shadow: 0px 0px 6px 2px rgba(0,0,0,0.2);
			padding:.2em .4em;
			margin:1em .5em;
			font-size:1.1em;
		}
		button:hover {
			background:#FFFFFF;
			box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.4);
		}
	</style>
  </head>
  <body>
	
	<div id="formButtons">
		<button onclick="window.location.href='http://erinavance.info/courses/WDV341/WDV341Homework/contactFormEmail.html'">Back to Page</button>
		<button onclick="window.location.href='https://bitbucket.org/EAVance/wdv341/src/724c8be78406d1add399277305d8601749013863/contactFormEmail.php?at=master&fileviewer=file-view-default'">View PHP</button>
	</div>
	
  </body>
</html>